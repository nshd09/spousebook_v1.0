<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0,width=device-width,user-scalable=no">
  <meta name="apple-mobile-web-app-capable" content="yes">
  <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">

  <title>Spousebook Dashboard</title>
  <link href="<?php echo e(url('admin_spo/css')); ?>/bootstrap.min.css" rel="stylesheet">
  <!-- Custom styles for this template -->
  <link href="<?php echo e(url('admin_spo/css')); ?>/datatable.bootstrap.min.css" rel="stylesheet">
  <link href="<?php echo e(url('admin_spo/css')); ?>/dashboard.css" rel="stylesheet">
  <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>

<body>

<header class="top-header">
  <div class="container-fluid">
    <div class="row">
      <div class="col-xs-8 col-sm-4">
        <i class="mobile-nav fa fa-bars"></i>
        <a href="#"><img class="logo-img" src="<?php echo e(url('admin_spo/images')); ?>/logo.png"></a>
      </div>
      <div class="col-xs-4 col-sm-8 text-right">
        <div class="btn-group username-btn">
          <span type="button" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <img src="<?php echo e(url('admin_spo/images')); ?>/user.jpg"> <span class="caret"></span>
          </span>
          <ul class="dropdown-menu dropdown-common dropdown-menu-right">
            <li><a href="#"><i class="fa fa-cog"></i>Setting</a></li>
            <li><a href="#"><i class="fa fa-pencil-square-o"></i>Edit Profile</a></li>
            <li><a href="#"><i class="fa fa-file-text-o"></i>View Profile</a></li>
            <li role="separator" class="divider"></li>
            <li><a href="#"><i class="fa fa-sign-out"></i>Log Out</a></li>
          </ul>
        </div>
      </div>
    </div>
  </div>
</header>

<div class="wrapper-box">
  <!-- //Left Box -->
  <div class="left-box">
    <div class="left-navigation">
      <ul id="collapsing" role="tablist" aria-multiselectable="true">
        <li class="panel panel-menu">
          <a href="#" class="dashboard-list active" data-toggle="collapse" data-parent="#collapsing">
            <i class="fa fa-dashboard"></i>Dashboard
          </a>
        </li>
        <li class="panel panel-menu">
          <a href="#" data-toggle="collapse" data-parent="#collapsing">
            <i class="fa fa-dashboard"></i>Member
          </a>
        </li>
        <li class="panel panel-menu">
          <a href="#accountmenu" class="more-menu collapsed" data-toggle="collapse" data-parent="#collapsing">
            <i class="fa fa-cube"></i>Account
            <span></span>
          </a>
          <ul class="collapse left-submenu" id="accountmenu">
            <li><a href="#" class="active-submenu"><i class="fa fa-cube"></i>Account-1</a></li>
            <li><a href="#"><i class="fa fa-cube"></i>Account-2</a></li>
            <li><a href="#"><i class="fa fa-cube"></i>Account-3</a></li>
          </ul>
        </li>
        <li class="panel panel-menu">
          <a href="#usermenu" class="more-menu collapsed" data-toggle="collapse" data-parent="#collapsing">
            <i class="fa fa-user"></i>User
            <span></span>
          </a>
          <ul class="collapse left-submenu" id="usermenu">
            <li><a href="#"><i class="fa fa-eye"></i>View</a></li>
            <li><a href="#"><i class="fa fa-file-o"></i>New</a></li>
          </ul>
        </li>
        <li class="panel panel-menu">
          <a href="#contactmenu" class="more-menu collapsed" data-toggle="collapse" data-parent="#collapsing">
            <i class="fa fa-phone-square"></i>Contact
            <span></span>
          </a>
          <ul class="collapse left-submenu" id="contactmenu">
            <li><a href="#"><i class="fa fa-area-chart"></i>Stats</a></li>
            <li><a href="#"><i class="fa fa-eye"></i>View</a></li>
            <li><a href="#"><i class="fa fa-file-o"></i>New</a></li>
          </ul>
        </li>
        <li class="panel panel-menu">
          <a href="#disputemenu" class="more-menu collapsed" data-toggle="collapse" data-parent="#collapsing">
            <i class="fa fa-hand-rock-o"></i>Dispute
            <span></span>
          </a>
          <ul class="collapse left-submenu" id="disputemenu">
            <li><a href="#"><i class="fa fa-area-chart"></i>Stats</a></li>
            <li><a href="#"><i class="fa fa-eye"></i>View</a></li>
            <li><a href="#"><i class="fa fa-file-o"></i>New</a></li>
          </ul>
        </li>
        <li class="panel panel-menu">
          <a href="#chatmenu" class="more-menu collapsed" data-toggle="collapse" data-parent="#collapsing">
            <i class="fa fa-comments-o"></i>Chat
            <span></span>
          </a>
          <ul class="collapse left-submenu" id="chatmenu">
            <li><a href="#"><i class="fa fa-comments-o"></i>Chat-1</a></li>
            <li><a href="#"><i class="fa fa-comments-o"></i>Chat-2</a></li>
          </ul>
        </li>
        <li class="panel panel-menu">
          <a href="#">
            <i class="fa fa-envelope-o"></i>Message
          </a>
        </li>
        <li class="panel panel-menu">
          <a href="#helpmenu">
            <i class="fa fa-info-circle"></i>Help
          </a>
        </li>
      </ul>
    </div>
  </div>


<?php echo $__env->yieldContent('content'); ?>




<script src="js/jquery.1.12.4.min.js"></script>
<script src="js/bootstrap.min.js"></script>

<!-- this is for datatable -->
<script src="js/datatable.min.js"></script>
<script src="js/datatable.bootstrap.min.js"></script>
<script type="text/javascript">
$(document).ready(function() {
  $('#example').DataTable();
});
</script>


<script src="js/common.js"></script>
</body>
</html>
