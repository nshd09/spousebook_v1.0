
<script type="text/ng-template" id="form-messages">
	  <div ng-message="required">This field cannot be blank</div>
	  <div ng-message="minlength">The value for this field is too short</div>
	  <div ng-message="maxlength">The value for this field is too long</div>
	  <div ng-message="email">You have entered an incorrect email value</div>
	  <div ng-message="pattern">You did not enter the value in the correct format</div>
	  <div ng-message="password-characters">
	    Your password must consist of alphabetical or numeric characters.
	    It must also contain at least one special character, a number as well as an uppercase letter.
	  </div>
</script>
              <form class="form-horizontal labelfont labelfont-abutme"  ng-controller="FormCtrl" ng-submit="submit(regisform.$valid)" novalidate autocomplete="off" id="registration">
                 <div class="sub-education">
                   <p class="center-heading-afterbefore">Basic Information</p>
                 </div>
                <div class="form-group">
                  <label class="col-sm-3 control-label">First Name :</label>
                  <div class="col-sm-8 mt7">
                   <input type="text" placeholder="First Name"  class="form-control" name="fname" value="<?php echo e($profiledata->fname); ?>" ng-model="fname" ng-minlength="2" ng-maxlength="20" required>
<!--                               <div ng-messages="regisform.fname.$error" ng-messages-include="form-messages" ng-if="interacted(regisform.fname)" class="error-messages">
                                    <div ng-message="required">This field is required</div>
                                    <div ng-message="minlength">Letter should be more than 1</div>
                                    <div ng-message="maxlength">Letter should be less than 20</div>
                                </div>-->
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-3 control-label">Last Name :</label>
                  <div class="col-sm-8 mt7">
                   <input type="text" placeholder="Last Name" class="form-control" name="lname" value="<?php echo e($profiledata->lname); ?>" ng-minlength="2" ng-maxlength="20" ng-model="lname" required >
                              
<!--                              <div ng-messages="regisform.lname.$error" ng-messages-include="form-messages" ng-if="interacted(regisform.lname)" class="error-messages">
                                <div ng-message="required">This field is required</div>
                                <div ng-message="minlength">Letter should be more than 1</div>
                                <div ng-message="maxlength">Letter should be less than 20</div>
                            </div>	-->
                  </div>
                </div>
                 <div class="form-group">
                  <label class="col-sm-3 control-label">Email :</label>
                  <div class="col-sm-8 mt7">
                      <input type="email" name="email" class="form-control" placeholder="Email address" id="emailId" ng-model="email" required value="<?php echo e($profiledata->email); ?>">
<!--                                <div ng-messages="regisform.email.$error" ng-messages-include="form-messages" ng-if="interacted(regisform.email)" class="error-messages">
                                        <div ng-message="required">This field is required</div>
                                  <div ng-message="email">Please enter correct email id</div>
                                      </div>-->
                             <span id="useremail"></span>
                  </div>
                </div>
                 <div class="form-group">
                  <label class="col-sm-3 control-label">Community :</label>
                  <div class="col-sm-8 mt7">
                    <select class="form-control" name="community_id" ng-model="community_id" required>
                                <option value='<?php echo e($profiledata->community_id); ?>' selected><?php echo e($profiledata->community_description); ?></option>
                                 <?php if(!empty($comunnity)): ?>
                                    <?php foreach($comunnity as $row): ?>
                                       <option value="<?php echo e($row->community_id); ?>"><?php echo e($row->community_description); ?></option>
                                    <?php endforeach; ?> 
                                <?php endif; ?>
                                  
                                 
                              </select>
<!--                              <div ng-messages="regisform.community_id.$error" ng-messages-include="form-messages" ng-if="interacted(regisform.community_id)" class="error-messages">
                                      <div ng-message="required">This field is required</div>
                                    </div>-->
                  </div>
                </div>
               
                <div class="form-group">
                  <div class="col-sm-8 col-sm-offset-3 mt10">
                    <button class="btn btn-warning active">Update</button>
                    <button class="btn btn-default active">Cancel</button>
                  </div>
                </div>
              </form>


