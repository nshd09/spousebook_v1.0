   <form class="form-horizontal labelfont labelfont-abutme" id="validateforms">
               <div class="sub-education mt30">
                <p class="center-heading-afterbefore">Education</p>
               </div>
               
                <div class="form-group">
                  <label class="col-sm-3 control-label">High School :</label>
                  <div class="col-sm-8 mt7">
                    <input type="text" class="form-control" placeholder="Completed – 2009-2010" name="">
                  </div>
                </div>

                <div class="form-group">
                  <label class="col-sm-3 control-label">Secondary :</label>
                  <div class="col-sm-8 mt7">
                    <input type="text" class="form-control" placeholder="Completed – 2009-2010" name="">
                  </div>
                </div>

                <div class="form-group">
                  <label class="col-sm-3 control-label">College: </label>
                  <div class="col-sm-8 mt7">
                    <input type="text" class="form-control" placeholder="Completed – 2009-2010" name="">
                  </div>
                </div>
                

                <div class="form-group">
                  <label class="col-sm-3 control-label">Visibility :</label>
                  <div class="col-sm-8 mt7">
                    <select class="form-control" name="post_type_id" ng-model="community_id" required>
                        <?php if($profiledata->post_type_id): ?>
                            <option value='<?php echo e($profiledata->post_type_id); ?>' selected><?php echo e($profiledata->post_type); ?></option>
                        <?php endif; ?>
                         <?php if(!empty($postcategory)): ?>
                            <?php foreach($postcategory as $rowpost): ?>
                               <option value="<?php echo e($rowpost->post_type_id); ?>"><?php echo e($rowpost->post_type); ?></option>
                            <?php endforeach; ?> 
                        <?php endif; ?>
                                  
                                 
                    </select>
                </div>
               <?php echo e(url('spo/images/cover-photo.jpg')); ?>

                <div class="form-group">
                  <div class="col-sm-8 col-sm-offset-3 mt10">
                    <button class="btn btn-warning active">Save changes</button>
                    <button class="btn btn-default active">Cancel</button>
                  </div>
                </div>
              </form>
