<?php $__env->startSection('content'); ?>
<?php echo $__env->make('includes.inner_profile_header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php echo $__env->make('includes.sub_menu', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
 <div class="background-white">
    <div class="row">
      <div class="col-sm-12">
         <fieldset class="scheduler-border">
          <legend class="scheduler-border">Community</legend>
          <p class="top-headingcontact">Family and Relationship Information</p>
          <div class="row">
            <div class="col-sm-8 col-sm-offset-2">
               <form class="form-horizontal labelfont">
                    <div class="form-group">
                      <div class="col-sm-8 col-sm-offset-3">
                      <div class="input_fields_wrapz">
                         <a class="btn btn-xs btn-default active add_field_button" data-addfieldclass=".input_fields_wrapz"><i class="fa fa-plus"></i> Add / Edit</a>
                      </div>
                      </div>
                    </div>

                    <div class="form-group">
                      <div class="col-sm-8 col-sm-offset-3">
                      <div class="input_fields_wrap">
                         <a class="btn btn-xs btn-default active add_field_button" data-addfieldclass=".input_fields_wrap"><i class="fa fa-plus"></i> Add names and addresses of family members</a>
                      </div>
                      </div>
                    </div>

                    <div class="form-group">
                      <label class="col-sm-3 control-label">Visibility :</label>
                      <div class="col-sm-8">
                        <select class="form-control">
                          <option>Public</option>
                          <option>Private</option>
                          <option>Group</option>
                         </select>
                      </div>
                    </div>

                    <div class="form-group">
                      <div class="col-sm-8 col-sm-offset-3">
                         <button class="btn btn-warning active">Save Changes</button>
                         <button class="btn btn-default active">Cancel</button>
                      </div>
                    </div>
                  </form>
             </div>
            </div>
         </fieldset>
      </div>
    </div>
    <br>
  </div>
</div>


<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>