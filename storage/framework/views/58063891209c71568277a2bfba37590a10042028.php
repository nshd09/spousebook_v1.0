<?php $__env->startSection('content'); ?>

<?php echo $__env->make('includes.inner_profile_header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php echo $__env->make('includes.setting_sub_menu', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<div id="newpage"><div class="background-white">
    <div class="row">
      <div class="col-sm-12 mt10">
        <fieldset class="scheduler-border">
          <legend class="scheduler-border">Change Password</legend>
          <!-- <p class="top-headingcontact">Contact Information</p> -->
          <div class="row">
            <div class="col-sm-8 col-sm-offset-2">
              <form class="form-horizontal labelfont labelfont-abutme" id="validateforms" ng-controller="FrmController">
                  <div class="form-group">
                    <label class="col-sm-3 control-label">Current Password :</label>
                    <div class="col-sm-8">
                     <input type="password" maxlength='12' name="oldpassword" ng-model="data.oldpassword" ng-minlength="6" ng-maxlength="12" password-characters-validator class="form-control" placeholder="Current Password" required>
                              <div ng-messages="regisform.oldpassword.$error" ng-if="interacted(regisform.password)" ng-messages-include="form-messages" class="error-messages">
                                <div ng-message="required">This field is required</div>
                                <div ng-message="minlength">Password length more than 5</div>
                                    <div ng-message="maxlength">Password length more than 12</div>
                                    <div ng-message="password-characters">
                                      Your password must consist of alphabetical or numeric characters.
                                       It must also contain at least one special character, a number as well as an uppercase letter.
                                    </div>
                              </div>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-3 control-label">New Password :</label>
                    <div class="col-sm-8">
                   <input type="password" maxlength='12' name="password" ng-model="data.password" ng-minlength="6" ng-maxlength="12" password-characters-validator class="form-control" placeholder="Password" required>
                              <div ng-messages="regisform.password.$error" ng-if="interacted(regisform.password)" ng-messages-include="form-messages" class="error-messages">
                                <div ng-message="required">This field is required</div>
                                <div ng-message="minlength">Password length more than 5</div>
                                    <div ng-message="maxlength">Password length more than 12</div>
                                    <div ng-message="password-characters">
                                      Your password must consist of alphabetical or numeric characters.
                                       It must also contain at least one special character, a number as well as an uppercase letter.
                                    </div>
                              </div>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-3 control-label" style="white-space: nowrap;">Re-enter Password :</label>
                    <div class="col-sm-8">
                         <input type="password"	
                                name="passConfirm"
                                ng-model="passConfirm"
                                ng-minlength="6"
             			ng-maxlength="12"
             			password-characters-validator
             			match-validator="data.password"
                                    class="form-control"
                                    placeholder="Re-enter Password" maxlength='12'>
                                        <div class="error-messages" ng-if="interacted(regisform.passConfirm)" ng-messages="regisform.passConfirm.$error" ng-messages-include="form-messages">
                                            <div ng-message="match">This password does not match the password entered before</div>
                                        </div>
                    </div>
                  </div>
                  <div class="form-group">
                    <div class="col-sm-8 col-sm-offset-3">
                      <button class="btn btn-warning active" ng-click='Submit();'>Save changes</button>
                      <button class="btn btn-default active">Cancel</button>
                    </div>
                  </div>
              </form>
            </div>
          </div>
        </fieldset>
      </div>
    </div>
  </div>
</div>
</div>

</div>

<?php $__env->stopSection(); ?>
 <script type="text/javascript">
                function FrmController($scope, $http) {
                    alert('sdfd');
                    $scope.errors = [];
                    $scope.msgs = [];
                    
                    $scope.SignUp = function() {

                        $scope.errors.splice(0, $scope.errors.length); // remove all error messages
                        $scope.msgs.splice(0, $scope.msgs.length);

                        $http.post('post_es.php', {'uname': $scope.username, 'pswd': $scope.userpassword, 'email': $scope.useremail}
                        ).success(function(data, status, headers, config) {
                            if (data.msg != '')
                            {
                                $scope.msgs.push(data.msg);
                            }
                            else
                            {
                                $scope.errors.push(data.error);
                            }
                        }).error(function(data, status) { // called asynchronously if an error occurs
    // or server returns response with an error status.
                            $scope.errors.push(status);
                        });
                    }
                }
            </script>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>