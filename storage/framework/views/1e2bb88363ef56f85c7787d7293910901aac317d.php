
              <form class="form-horizontal labelfont labelfont-abutme" id="validateforms">
                 <div class="sub-education">
                   <p class="center-heading-afterbefore">Work</p>
                 </div>
                <div class="form-group">
                  <label class="col-sm-3 control-label">Company :</label>
                  <div class="col-sm-8 mt7">
                   <input type="text" name="Company" class="form-control" placeholder="Company">
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-3 control-label">Position held :</label>
                  <div class="col-sm-8 mt7">
                  <input type="text" name="Company" class="form-control" placeholder="Position held">
                  </div>
                </div>
                 <div class="form-group">
                  <label class="col-sm-3 control-label">Country :</label>
                  <div class="col-sm-8 mt7">
                    <input type="text" name="Company" class="form-control" placeholder="Country">
                  </div>
                </div>
                 <div class="form-group">
                  <label class="col-sm-3 control-label">City/Town :</label>
                  <div class="col-sm-8 mt7">
                    <input type="text" class="form-control" name="" placeholder="City/Town">
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-3 control-label">Role :</label>
                  <div class="col-sm-8 mt7">
                    <input type="text" class="form-control" name="" placeholder="Role">
                  </div>
                </div>
                <div class="form-group mb0">
                  <label class="col-sm-3 control-label">Duration :</label>
                  <div class="col-sm-8 mt7">
                    <input type="text" class="form-control datepick" name="" placeholder="Duration">
                    <label class="mt7"><input type="checkbox" name=""> Current Job</label>
                  </div>
                </div>

                 <div class="form-group">
                    <label class="col-sm-3 control-label">Visibility :</label>
                    <div class="col-sm-8 mt7">
                        <select class="form-control">
                          <option>Public</option>
                          <option>Private</option>
                          <option>Group</option>
                        </select>
                    </div>
                 </div>
               
                <div class="form-group">
                  <div class="col-sm-8 col-sm-offset-3 mt10">
                    <button class="btn btn-warning active">Save changes</button>
                    <button class="btn btn-default active">Cancel</button>
                  </div>
                </div>
              </form>


