<?php $__env->startSection('content'); ?>
<?php echo $__env->make('includes.inner_profile_header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php echo $__env->make('includes.sub_menu', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
 <div class="background-white">
    <div class="row">
      <div class="col-sm-12">
         <fieldset class="scheduler-border">
          <legend class="scheduler-border">Groups</legend>
          <div class="row">
            <div class="col-sm-8 col-sm-offset-2">
               <form class="form-horizontal labelfont">
                    <div class="form-group">
                      <label class="col-sm-3 control-label">Create Groups :<strong>*</strong></label>
                      <div class="col-sm-8">
                        <input type="text" class="form-control" placeholder="Create Groups">
                      </div>
                    </div>

                    <div class="form-group">
                      <label class="col-sm-3 control-label">Address Line1 :<strong>*</strong></label>
                      <div class="col-sm-8">
                        <input type="text" class="form-control" placeholder="Address">
                      </div>
                    </div>

                    <div class="form-group">
                      <label class="col-sm-3 control-label">Line2 :<strong>*</strong></label>
                      <div class="col-sm-8">
                        <input type="text" class="form-control" placeholder="Address">
                      </div>
                    </div>

                    <div class="form-group">
                      <label class="col-sm-3 control-label">Line3 :</label>
                      <div class="col-sm-8">
                        <input type="text" class="form-control" placeholder="Address">
                      </div>
                    </div>
                    
                    <div class="form-group">
                      <label class="col-sm-3 control-label">Country :<strong>*</strong></label>
                      <div class="col-sm-8">
                        <select class="form-control">
                          <option>India</option>
                          <option>Nepal</option>
                          <option>China</option>
                         </select>
                      </div>
                    </div>

                    <div class="form-group">
                      <label class="col-sm-3 control-label">City/State :<strong>*</strong></label>
                      <div class="col-sm-8">
                        <input type="text" class="form-control" placeholder="City/State">
                      </div>
                    </div>

                    <div class="form-group">
                      <label class="col-sm-3 control-label">Province :<strong>*</strong></label>
                      <div class="col-sm-3">
                        <input type="text" class="form-control" placeholder="Province">
                      </div>
                       <label class="col-sm-2 control-label" style="white-space:nowrap;">Postal Code :<strong>*</strong></label>
                      <div class="col-sm-3">
                        <input type="text" class="form-control" placeholder="Postal Code">
                      </div>
                    </div>

                    <div class="form-group">
                      <div class="col-sm-8 col-sm-offset-3">
                      <div>
                         <a class="btn btn-xs btn-default active add_field_button"><i class="fa fa-plus"></i> Click to add more</a>
                      </div>
                      </div>
                    </div>

                    <div class="form-group">
                      <div class="col-sm-8 col-sm-offset-3">
                         <button class="btn btn-warning active">Save Changes</button>
                         <button class="btn btn-default active">Cancel</button>
                      </div>
                    </div>
                  </form>
              </div>
            </div>
         </fieldset>
      </div>
     </div>
    <br>
  </div>
</div>



<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>