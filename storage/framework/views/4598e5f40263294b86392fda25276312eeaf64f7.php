<?php $__env->startSection('content'); ?>
 <!-- //Left Box -->
  <div class="right-box g">
    <div class="right-box-inner">
      <div class="row">
        <div class="col-sm-12">
          <ol class="breadcrumb">
            <li>
              <a href="#">Home</a>
            </li>
            <li class="active">Dashboard</li>
          </ol>
        </div>
      </div>

      <div class="row">
        <div class="col-sm-12">
          <div>
            <ul class="nav nav-tabs nav-tabs-common" role="tablist">
              <li role="presentation" class="active"><a href="#datagrid" role="tab" data-toggle="tab">Data Grid</a></li>
              <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">From Validation</a></li>
              <li role="presentation"><a href="#messages" aria-controls="messages" role="tab" data-toggle="tab">Messages</a></li>
              <li role="presentation"><a href="#settings" aria-controls="settings" role="tab" data-toggle="tab">Settings</a></li>
            </ul>
            <!-- Tab panes -->
            <div class="tab-content tab-content-common">
              <div role="tabpanel" class="tab-pane fade in active" id="datagrid">
                <table id="example" class="table table-striped table-bordered table-action" cellspacing="0" width="100%">
                  <thead>
                    <tr>
                      <th>First name</th>
                      <th>Last name</th>
                      <th>Position</th>
                      <th width="60" data-orderable="false">Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td>Alexa</td>
                      <td>Nixon</td>
                      <td>System Architect</td>
                      <td>
                        <button class="btn btn-xs btn-primary glyphicon glyphicon-edit" title="Edit"></button>
                        <button class="btn btn-xs btn-danger glyphicon glyphicon-trash" title="Delete"></button>
                      </td>
                    </tr>
                    <tr>
                      <td>Garrett</td>
                      <td>Winters</td>
                      <td>Accountant</td>
                      <td>
                        <button class="btn btn-xs btn-primary glyphicon glyphicon-edit" title="Edit"></button>
                        <button class="btn btn-xs btn-danger glyphicon glyphicon-trash" title="Delete"></button>
                      </td>
                    </tr>
                    <tr>
                      <td>Ashton</td>
                      <td>Cox</td>
                      <td>Junior Technical Author</td>
                      <td>
                        <button class="btn btn-xs btn-primary glyphicon glyphicon-edit" title="Edit"></button>
                        <button class="btn btn-xs btn-danger glyphicon glyphicon-trash" title="Delete"></button>
                      </td>
                    </tr>
                    <tr>
                      <td>Cedric</td>
                      <td>Kelly</td>
                      <td>Senior Javascript Developer</td>
                      <td>
                        <button class="btn btn-xs btn-primary glyphicon glyphicon-edit" title="Edit"></button>
                        <button class="btn btn-xs btn-danger glyphicon glyphicon-trash" title="Delete"></button>
                      </td>
                    </tr>
                    <tr>
                      <td>Airi</td>
                      <td>Satou</td>
                      <td>Accountant</td>
                      <td>
                        <button class="btn btn-xs btn-primary glyphicon glyphicon-edit" title="Edit"></button>
                        <button class="btn btn-xs btn-danger glyphicon glyphicon-trash" title="Delete"></button>
                      </td>
                    </tr>
                    <tr>
                      <td>Brielle</td>
                      <td>Williamson</td>
                      <td>Integration Specialist</td>
                      <td>
                        <button class="btn btn-xs btn-primary glyphicon glyphicon-edit" title="Edit"></button>
                        <button class="btn btn-xs btn-danger glyphicon glyphicon-trash" title="Delete"></button>
                      </td>
                    </tr>
                    <tr>
                      <td>Herrod</td>
                      <td>Chandler</td>
                      <td>Sales Assistant</td>
                      <td>
                        <button class="btn btn-xs btn-primary glyphicon glyphicon-edit" title="Edit"></button>
                        <button class="btn btn-xs btn-danger glyphicon glyphicon-trash" title="Delete"></button>
                      </td>
                    </tr>
                    <tr>
                      <td>Rhona</td>
                      <td>Davidson</td>
                      <td>Integration Specialist</td>
                      <td>
                        <button class="btn btn-xs btn-primary glyphicon glyphicon-edit" title="Edit"></button>
                        <button class="btn btn-xs btn-danger glyphicon glyphicon-trash" title="Delete"></button>
                      </td>
                    </tr>
                    <tr>
                      <td>Colleen</td>
                      <td>Hurst</td>
                      <td>Javascript Developer</td>
                      <td>
                        <button class="btn btn-xs btn-primary glyphicon glyphicon-edit" title="Edit"></button>
                        <button class="btn btn-xs btn-danger glyphicon glyphicon-trash" title="Delete"></button>
                      </td>
                    </tr>
                    <tr>
                      <td>Sonya</td>
                      <td>Frost</td>
                      <td>Software Engineer</td>
                      <td>
                        <button class="btn btn-xs btn-primary glyphicon glyphicon-edit" title="Edit"></button>
                        <button class="btn btn-xs btn-danger glyphicon glyphicon-trash" title="Delete"></button>
                      </td>
                    </tr>
                    <tr>
                      <td>Jena</td>
                      <td>Gaines</td>
                      <td>Office Manager</td>
                      <td>
                        <button class="btn btn-xs btn-primary glyphicon glyphicon-edit" title="Edit"></button>
                        <button class="btn btn-xs btn-danger glyphicon glyphicon-trash" title="Delete"></button>
                      </td>
                    </tr>
                    <tr>
                      <td>Quinn</td>
                      <td>Flynn</td>
                      <td>Support Lead</td>
                      <td>
                        <button class="btn btn-xs btn-primary glyphicon glyphicon-edit" title="Edit"></button>
                        <button class="btn btn-xs btn-danger glyphicon glyphicon-trash" title="Delete"></button>
                      </td>
                    </tr>
                    <tr>
                      <td>Charde</td>
                      <td>Marshall</td>
                      <td>Regional Director</td>
                      <td>
                        <button class="btn btn-xs btn-primary glyphicon glyphicon-edit" title="Edit"></button>
                        <button class="btn btn-xs btn-danger glyphicon glyphicon-trash" title="Delete"></button>
                      </td>
                    </tr>
                    <tr>
                      <td>Haley</td>
                      <td>Kennedy</td>
                      <td>Senior Marketing Designer</td>
                      <td>
                        <button class="btn btn-xs btn-primary glyphicon glyphicon-edit" title="Edit"></button>
                        <button class="btn btn-xs btn-danger glyphicon glyphicon-trash" title="Delete"></button>
                      </td>
                    </tr>
                    <tr>
                      <td>Tatyana</td>
                      <td>Fitzpatrick</td>
                      <td>Regional Director</td>
                      <td>
                        <button class="btn btn-xs btn-primary glyphicon glyphicon-edit" title="Edit"></button>
                        <button class="btn btn-xs btn-danger glyphicon glyphicon-trash" title="Delete"></button>
                      </td>
                    </tr>
                    <tr>
                      <td>Michael</td>
                      <td>Silva</td>
                      <td>Marketing Designer</td>
                      <td>
                        <button class="btn btn-xs btn-primary glyphicon glyphicon-edit" title="Edit"></button>
                        <button class="btn btn-xs btn-danger glyphicon glyphicon-trash" title="Delete"></button>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>
              <div role="tabpanel" class="tab-pane fade" id="profile">
                <form class="form-horizontal">
                  <div class="form-group">
                    <label for="inputEmail3" class="col-sm-3 control-label">Email:</label>
                    <div class="col-sm-4">
                      <input type="email" class="form-control" id="inputEmail3" placeholder="Email">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputPassword3" class="col-sm-3 control-label">Password:</label>
                    <div class="col-sm-4">
                      <input type="password" class="form-control" id="inputPassword3" placeholder="Password">
                    </div>
                  </div>
                  <div class="form-group">
                    <div class="col-sm-offset-3 col-sm-10">
                      <div class="checkbox">
                        <label>
                          <input type="checkbox"> Remember me
                        </label>
                      </div>
                    </div>
                  </div>
                  <div class="form-group">
                    <div class="col-sm-offset-3 col-sm-9">
                      <button type="submit" class="btn btn-default">Sign in</button>
                    </div>
                  </div>
                </form>
              </div>
              <div role="tabpanel" class="tab-pane fade" id="messages">3.</div>
              <div role="tabpanel" class="tab-pane fade" id="settings">4.</div>
            </div>
          </div>
          
        </div>
      </div>

    </div>
  </div>
</div>



<script src="js/jquery.1.12.4.min.js"></script>
<script src="js/bootstrap.min.js"></script>

<!-- this is for datatable -->
<script src="js/datatable.min.js"></script>
<script src="js/datatable.bootstrap.min.js"></script>
<script type="text/javascript">
$(document).ready(function() {
  $('#example').DataTable();
});
</script>


<script src="js/common.js"></script>
</body>
</html>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.admin_app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>