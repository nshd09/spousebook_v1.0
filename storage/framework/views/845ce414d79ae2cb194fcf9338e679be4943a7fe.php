Hello <?php echo e($user->name); ?>,
<br>

<h4>Click here to reset your password: </h4>
<a href="<?php echo e($link = url('password/reset', $token).'?email='.urlencode($user->getEmailForPasswordReset())); ?>"> <?php echo e($link); ?> </a>