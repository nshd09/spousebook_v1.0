<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'UsersController@index');
Route::get('pages/about', 'UsersController@about');
Route::get('pages/privacy', 'UsersController@privacy');
Route::get('pages/terms', 'UsersController@terms');
Route::post('users/register', 'UsersController@register');

Route::get('checkEmail/{email}', 'UsersController@checkEmail');

Route::auth();
Route::group(['middleware' => 'auth'], function()
{
Route::get('/home', 'HomeController@index');
Route::get('/{name}', 'UsersController@profile');
Route::get('user/pages/{pagename}', 'UsersController@pages');
Route::get('/setting/{name}', 'UsersController@setting');
Route::get('/community/{name}', 'HomeController@community');
Route::get('/group/{name}', 'HomeController@group');

    
});
Route::auth();




