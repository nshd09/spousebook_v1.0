<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;

use Validator, Redirect; 
use Schema;
use App\User;
use App\Community;
use App\UserProfile;
use Illuminate\Http\Request;
use Session;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Auth;
use Mail;
use App\Libraries\Helpers;

class UsersController extends Controller
{

//    public function __construct(){
//        $this->middleware('auth');
//    }

    
/**
* This function for open front page.
*@Auther Ahsan Ahaamad 
* @date 13-07-2016
*/
    public function index()
    {
        if (!Auth::check()) {
        $comunnity  = DB::table('communities')->where('status',1)->select('community_id','community_description')->paginate();
        return view('welcome',compact('comunnity'));
        }else{
            return redirect('/home');
        }
    }

/**
* This function for register.
*@Auther Ahsan Ahaamad 
* @date 13-07-2016
*/
    public function register(Request $request)
    {
      
        if($request->ajax()){
            $data = $request->input();
            
           	$uservalue['email'] = $data['email'];
                $uservalue['password'] = bcrypt($data['password']);
                $uservalue['role_id'] = $data['role_id'];
                $uservalue['community_id'] = $data['community_id'];
                $uservalue['created_at'] = date('Y-m-d h:i:a');
                $uservalue['remember_token'] = $data['remember_token'];
              
                $userdata =  User::create($uservalue);
                
                $userupdate = strtolower($data['fname'].'.'.$data['lname']).'.'.$userdata->id;
                
                 $ch = DB::table('users')->where('id',$userdata->id)->update(array('name' => $userupdate));
                
                $userprofilevalue['user_id'] = $userdata->id;
                $userprofilevalue['fname'] = $data['fname'];
                $userprofilevalue['lname'] = $data['lname'];
                $userprofilevalue['dob'] = $data['dob'];
                $userprofilevalue['gender'] = $data['gender'];
                $userprofilevalue['created_at'] = date('Y-m-d h:i:a');
                $profiledata =  UserProfile::create($userprofilevalue);
                
                $data = [$data['fname'], $data['lname'],];
                $message = 'i am hear';
            if($userdata){
                //Mail::send('emails.welcome', $data, function ($message) {
                    //$message->from('ahsan.ahmad@xipetech.com', 'Registration has benn successful');
                   // $message->to($data['email'])->subject('Welcome Spousebook ');

               // });

            if (Auth::attempt([ 'email' => $request['email'], 'password' => $request['password'] ])){
                Session::flash('message','User has been added successfully.');
               return 'succ';
            }
        }
      }
        
    }
    
/**
* This function for check unique email id.
*@Auther Ahsan Ahaamad 
* @date 14-07-2016
*/

    public function checkEmail($email){
        
        $getEmail = DB::Table('users')->where('email',$email)->first();
        if(isset($getEmail)){
            $data = 'exist';
        }else{
             $data = 'notexist';
        }
        return $data;
    }

    
/**
* This function for .
*@Auther Ahsan Ahaamad 
* @date 13-07-2016
*/
  
    public function profile($name = null){
        
        $profiledata = DB::table('users')
            ->join('user_profiles', 'users.id', '=', 'user_profiles.user_id')->join('communities','users.community_id','=','communities.community_id')->where('user_id',Auth::user()->id)->first();
        $userProfile = DB::Table('user_profiles')->where('user_id',Auth::user()->id)->first();
//        echo '<pre>';
//        print_r($profiledata);exit;
        
        
        return view('users.profile',compact('profiledata','userProfile'));
    }
    
    public function pages($name = null){
        
        $comunnity  = DB::table('communities')->where('status',1)->select('community_id','community_description')->paginate();
        $profiledata = DB::table('users')
            ->join('user_profiles', 'users.id', '=', 'user_profiles.user_id')->join('communities', 'users.community_id', '=', 'communities.community_id')->join('post_types', 'user_profiles.post_type_id', '=', 'post_types.post_type_id')->where('id',Auth::user()->id)->first();
        
        $comunnity  = DB::table('communities')->where('status',1)->select('community_id','community_description')->paginate();
        $postcategory  = DB::table('post_types')->where('status',1)->select('post_type_id','post_type')->paginate();
       
        return view('users.'.$name,compact('profiledata','comunnity','postcategory'));
    }
     public function setting(){
        $userProfile = DB::Table('user_profiles')->where('user_id',Auth::user()->id)->first();
        //$photos = DB::Table('galleries')->join('module_categories','galleries.module_category_id','=','module_categories.module_category_id')->Where('galleries.user_id',Auth::user()->id)->get();
        return view('users.setting',compact('userProfile'));
    }


    public function about(){
        return view('about');
    }
    public function privacy(){
        return view('privacy');
    }
    public function terms(){
         return view('terms');
    }




}

