<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Traits\FileUploadTrait;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

abstract class Controller extends BaseController
{
    
//     public function __construct(){
//	      $this->middleware('auth');
//     }

    use AuthorizesRequests, DispatchesJobs, ValidatesRequests, FileUploadTrait;
}
