<?php

namespace App\Http\Controllers;
use App\User;
use App\OrderDetails;
use App\Http\Requests;
use Illuminate\Http\Request;
use DB;


use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

/**
* This function for after login home page.
*@Auther Ahsan Ahaamad 
* @date 13-07-2016
*/
    public function index()
    {
        $userProfile = DB::Table('user_profiles')->where('user_id',Auth::user()->id)->first();
        
        
        return view('home',compact('userProfile'));
    }
    
    public function community()
    {
       
        $userProfile = DB::Table('user_profiles')->where('user_id',Auth::user()->id)->first();
        return view('users.community',compact('userProfile'));
    }
    
    public function group()
    {
       
        $userProfile = DB::Table('user_profiles')->where('user_id',Auth::user()->id)->first();
        return view('users.group',compact('userProfile'));
    }
}
