@extends('layouts.app')
@section('content')
<div class="container">
	<div class="row">
		<div class="col-sm-12">
		<div class="about-us-bg">
					<h1 class="text-theme text-black-shadow text-center">Terms & Condition</h1>
					<p>
						
						<h3>Spousebook Terms & Agreement of Website Usage</h3> 
						</p>
<p>
This User Agreement governs the use of the Spousebook site service. Your use of the Service will constitute your agreement to comply with these rules. If you do not agree with the rules contained in this Agreement, please do not use the Service.<br><br>

The following rule(s) may be modified by service provider from time to time. Continued use of the service by you will constitute your acceptance of any changes or revisions to this agreement.<br><br>

The information presented at our website is to enable you make an informed decision whether or not you want to use the service. These services are available to address non-urgent questions of a general nature. If you have any concerns about anything otherwise, please consult the appropriate agency immediately or you can reach our support service department.<br><br>

None of the information presented at our website should be considered a substitute for personal request.<br><br>

Individuals who desire to share their relationship experiences, make contact with friends and families, make new friends or seek counsel are the primary beneficiaries of the services of this website.<br><br>

Other information posted here is for public consumption and will therefore be attended to by addresses and links attached to them.<br><br>

Views expressed by members and/or counselors are not the views of Spousebook.<br><br>

Any member or counselor understands that any information, professional advice or decision(s) taken or given remain information, professional advice or decision given or taken by those involved, and do not reflect the official stand of Spousebook and her affiliates.<br><br>

Members dealing with counselors outside of our platform, do so at their own risk and in a client-professional relationship.<br><br>

Spousebook does not endorse any particular member or counselor for those seeking help. The choice of counselor remains a personal choice of members.<br><br>

Members and counselors who feel abused by any entity may report such abuses, giving full written details to either Spousebook, the law enforcement agencies or both.

Arbitration: Any issue of legal nature will be amicably settled or referred to the appropriate court of law at cost of those filing such suits. 
					</p>

				<br><br><br><br><br><br>
					</div>
		 </div>


				
				
			</div>

		</div>
	</div>
</div>

@endsection