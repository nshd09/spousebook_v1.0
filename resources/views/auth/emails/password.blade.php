Hello {{$user->name}},
<br>

<h4>Click here to reset your password: </h4>
<a href="{{ $link = url('password/reset', $token).'?email='.urlencode($user->getEmailForPasswordReset()) }}"> {{ $link }} </a>