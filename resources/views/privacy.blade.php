@extends('layouts.app')
@section('content')
<div class="container">
	<div class="row">
		<div class="col-sm-12">
		<div class="about-us-bg">
					<h1 class="text-theme text-black-shadow text-center">Privacy</h1>
					<p class="mt30">
						This Privacy Policy is intended to inform you of Spousebook policies and practices regarding the collection, use and disclosure of any Personal Information you submit to us through our website. Privacy of visitors to our website which is located at www.spousebooks.com is highly important to us. You are the owner of information you post at the website using the tools that are available to you.<br><br>

Spusebook’s security feature allows you to set your own privacy settings based on your personal preference. Spousebook do not share personal information about you with other people or nonaffiliated organizations, except to provide products or services you have requested, when we have your permission, or as otherwise described in this privacy policy. We may disclose information, if we believe it is necessary to investigate, prevent, or take action regarding suspected illegal activities or fraud, situations involving the physical safety of any person, violations of Spousebook’s terms of use, or as otherwise required by applicable law.<br><br>

Profile of counselors and the service they provide are geared toward improving relationship cultures, therefore, users are advised to check the counselor’s profile before initiating connection with them. On the other hand, Spousebook collaborates with these counselors to aid our platform and to make our aspirations come to fruition. Request by individuals to join the Spousebook network is permitted and shall therefore remain so until management in future thinks otherwise.<br>
</p>
<h3>Third Party Advertising</h3> 
<p>
Spousebook uses third-party advertising companies to provide advertisements when you visit our website. These companies may use Personal Information, such as gender or age, and other descriptive information in order to provide advertisements at the website about goods and services that may be of interest to you. As you navigate the website, certain information may also be passively collected and stored on our server logs, including your Internet protocol address, browser type, and operating system. We also use navigational data like Uniform Resource Locators (URL) and cookies to gather information regarding the date and time of your visit, solutions and information for which you searched and viewed or on some of the advertisements displayed at the website you clicked. These types of information are collected to make our services and solutions more useful to you and to tailor the Spousebook community experience to meet your special interests and needs. 
					</p>

				<br><br><br><br><br>
					</div>
		 </div>


				
				
			</div>

		</div>
	</div>
</div>

@endsection