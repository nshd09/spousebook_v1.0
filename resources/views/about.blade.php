@extends('layouts.app')
@section('content')
<div class="container">
	<div class="row">
		<div class="col-sm-12">
		<div class="about-us-bg">
					<h1 class="text-theme text-black-shadow text-center">About Us</h1>
					<p class="mt30">
						Spousebook is birthed by a small team of social entrepreneurs with vast experience in and
            exposure to the management of non-profit organizations, as well as the development
            websites.<br><br>
            Spousebook is the world&#39;s latest social media platform that offers rare opportunities to and
            serves as a resource for individuals who want to seek professional counsel on any of the multi-
            faceted dimensions of spousal issues.<br><br>
            <strong>Mission:</strong> To implement a social networking platform that addresses various challenges in
            relationships.<br><br>
           <strong>Vision:</strong> To sufficiently influence a positive change in spousal relationships.
		           <ol>
		           	<li>We believe that if we are positive in our thinking, we own the power for a successful
		           future</li>
		           	<li>We believe that our world is joyous when we build positive relationships</li>
		           	<li>We believe that the needed revisions we make today in our relationships will have
		            rippling effect of socioeconomic advancement across families and more opportunities
		            for improving livelihoods</li>
		           	<li>We believe that every individual owns the power and drive to shape his/her own destiny</li>
		           </ol>
              <br>
              <p>
              	 Spousebook is more than the sum of its parts. We offer a focused platform that believes in
              information sharing.
              </p>
             
					</p>

				<br><br><br><br><br><br><br><br><br><br><br><br>
					</div>
		 </div>


				
				
			</div>

		</div>
	</div>
</div>

@endsection