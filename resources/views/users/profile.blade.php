@extends('layouts.app')

@section('content')

@include('includes.profile_header')
@include('includes.sub_menu')

<div class="background-white">
    <div class="row">
      <div class="col-sm-12 mt10">
        <fieldset class="scheduler-border">
          <legend class="scheduler-border">About Me</legend>
          <!-- <p class="top-headingcontact">Contact Information</p> -->
          <div class="row">
            <div class="col-sm-8 col-sm-offset-2" >
               <form class="form-horizontal labelfont labelfont-abutme" id="basicinfo">
                 <div class="set-editbutton mt30">
                  <a href="#"><span class="fa fa-edit pageId" data-page='basicinfo' data-id='pageId'> Edit</a>  
               </div>
              <div class="sub-education mt30">
                <p class="center-heading-afterbefore">Basic Information</p>
               </div>
                <div class="form-group">
                  <label class="col-sm-3 control-label">First Name</label>
                  <div class="col-sm-8 mt7">
                     <small> {{ $profiledata->fname}}</small>
                  </div>
                </div>

                
                <div class="form-group">
                  <label class="col-sm-3 control-label">Last Name: </label>
                  <div class="col-sm-8 mt7">
                     <small> {{ $profiledata->lname}}</small>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-3 control-label">Email :</label>
                  <div class="col-sm-8 mt7">
                     <small> {{ $profiledata->email}}</small>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-3 control-label">Community :</label>
                  <div class="col-sm-8 mt7">
                     <small> {{ $profiledata->community_description}}</small>
                  </div>
                </div>
               
              </form>
              <form class="form-horizontal labelfont labelfont-abutme" id='work'>
                   <div class="set-editbutton">
                  <a href="#" ><span class="fa fa-edit pageId" data-page='work' data-id='pageId'  > Edit</a>  
               </div>
               <div class="sub-education">
                <p class="center-heading-afterbefore">Work</p>
               </div>
                <div class="form-group">
                  <label class="col-sm-3 control-label">Company :</label>
                  <div class="col-sm-8 mt7">
                    <small>Xipe-Tech</small>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-3 control-label">Position held :</label>
                  <div class="col-sm-8 mt7">
                   <small>Developer</small>
                  </div>
                </div>
                 <div class="form-group">
                  <label class="col-sm-3 control-label">Country :</label>
                  <div class="col-sm-8 mt7">
                   <small>India</small>
                  </div>
                </div>
                 <div class="form-group">
                  <label class="col-sm-3 control-label">City/Town :</label>
                  <div class="col-sm-8 mt7">
                    <small>Lucknow</small>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-3 control-label">Role :</label>
                  <div class="col-sm-8 mt7">
                     <small>Lucknow</small>
                  </div>
                </div>
                <div class="form-group mb0">
                  <label class="col-sm-3 control-label">Duration :</label>
                  <div class="col-sm-8 mt7">
                    <small>1995 To 2016</small>
                    <label class="mt7"><input type="checkbox" name=""> Current Job</label>
                  </div>
                </div>

                 <div class="form-group">
                    <label class="col-sm-3 control-label">Visibility :</label>
                    <div class="col-sm-8 mt7">
                      <small> Public</small>
                    </div>
                 </div>
              </form>

              <form class="form-horizontal labelfont labelfont-abutme" id='professional_skills'>
               <div class="set-editbutton">
                  <a href="#"><span class="fa fa-edit pageId" data-page='professional_skills.' data-id='pageId'> Edit</a>  
               </div>
                <div class="sub-education mt30">
                <p class="center-heading-afterbefore">Professional Skills</p>
               </div>
                <div class="form-group">
                  <label class="col-sm-3 control-label">Professional Skills :</label>
                  <div class="col-sm-8 mt7">
                    <input type="text" class="form-control" placeholder="Type a professional skills acquired" name="">
                  </div>
                </div>
                

                <div class="form-group">
                  <label class="col-sm-3 control-label">Visibility :</label>
                  <div class="col-sm-8 mt7">
                    <small> Public</small>
                  </div>
                </div>
              </form>
              
             <form class="form-horizontal labelfont labelfont-abutme" id="education">
                 <div class="set-editbutton mt30">
                  <a href="#"><span class="fa fa-edit pageId" data-page='education' data-id='pageId'> Edit</a>  
               </div>
              <div class="sub-education mt30">
                <p class="center-heading-afterbefore">Education</p>
               </div>
                <div class="form-group">
                  <label class="col-sm-3 control-label">High School :</label>
                  <div class="col-sm-8 mt7">
                     <small> Completed – 2009-2010</small>
                  </div>
                </div>

                <div class="form-group">
                  <label class="col-sm-3 control-label">Secondary :</label>
                  <div class="col-sm-8 mt7">
                     <small> Completed – 2009-2010</small>
                  </div>
                </div>

                <div class="form-group">
                  <label class="col-sm-3 control-label">College: </label>
                  <div class="col-sm-8 mt7">
                     <small> Completed – 2009-2010</small>
                  </div>
                </div>
                

                <div class="form-group">
                  <label class="col-sm-3 control-label">Visibility :</label>
                  <div class="col-sm-8 mt7">
                    <small> Public</small>
                  </div>
                </div>
              </form>

            </div>
          </div>
        </fieldset>
      </div>
    </div>
  </div>
</div>

@endsection
