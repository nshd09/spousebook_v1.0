 <form class="form-horizontal labelfont labelfont-abutme" id="validateforms">
               <div class="sub-education mt30">
                <p class="center-heading-afterbefore">Professional Skills</p>
               </div>
                <div class="form-group">
                  <label class="col-sm-3 control-label">Professional Skills :</label>
                  <div class="col-sm-8 mt7">
                    <input type="text" class="form-control" placeholder="Type a professional skills acquired" name="">
                  </div>
                </div>
                
               <div class="form-group">
                    <label class="col-sm-3 control-label">Visibility :</label>
                    <div class="col-sm-8 mt7">
                        <select class="form-control">
                          <option>Public</option>
                          <option>Private</option>
                          <option>Group</option>
                        </select>
                    </div>
                 </div>
               
                <div class="form-group">
                  <div class="col-sm-8 col-sm-offset-3 mt10">
                    <button class="btn btn-warning active">Save changes</button>
                    <button class="btn btn-default active">Cancel</button>
                  </div>
                </div>
              </form>
