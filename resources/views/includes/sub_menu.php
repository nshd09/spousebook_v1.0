 <div class="row">
    <div class="col-sm-12 mt15">
      <div class="secnd-menu">
        <span class="glyphicon glyphicon-option-vertical scnd-mob-nav"></span>
        <div class="topnav">
          <ul>
            <li><a href="<?php echo url('/',Auth::user()->name);?>">About Me</a></li>
            <li><a href="<?php echo url('/',Auth::user()->name);?>">Basic Information</a></li>
            <li><a href="<?php echo url('/community',Auth::user()->name);?>">Community</a></li>
            <li><a href="<?php echo url('/group',Auth::user()->name);?>">Groups</a></li>
            <li><a href="#contact">Adventures</a></li>
          </ul>
        </div>
        <div class="search-right">
          <form class="form-inline">
             <input type="text" placeholder="Find Friends">
             <button type="button" class="btn-friendsearch">Search</button>
          </form>
        </div>
      </div>
    </div>
  </div>
