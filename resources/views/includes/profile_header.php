 <link rel="stylesheet" href="{{ url('spo/css') }}/mobile.css"/>

<div class="image-set"></div>
<div class="container">
  <div class="row">
    <div class="col-sm-12 mt15">
      <div class="header-content" style="background:url('spo/images/cover-photo.jpg') no-repeat;">
        <div class="profile-img">
          <img src="spo/images/profile.jpg" class="img-responsive" alt="">
        </div>
        <div class="profile-right">
          <div class="section-left">
            <h1><?php echo ucfirst($userProfile->fname).' '.ucfirst($userProfile->lname);?></h1>
          </div>
          <div class="section-right">
            <a href="#" class="btn btn-default btn-sm">Update Photo</a>
            <a href="#" class="btn btn-default btn-sm">Activity Log</a>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="row">
    <div class="col-sm-12">
      <div class="header-bottom-menu">
        <span class="glyphicon glyphicon-option-vertical menusimple-mob-nav"></span>
        <div class="menu_simple">
          <ul>
            <li><a href="#">Friends</a></li>
            <li><a href="#" class="current">My Posts</a></li>
            <li><a href="#">Photo Album</a></li>
            <li><a href="#">Extras</a></li>
          </ul>
        </div>
      </div>
    </div>
  </div>


 