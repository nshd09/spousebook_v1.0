<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0,width=device-width,user-scalable=no">
  <meta name="apple-mobile-web-app-capable" content="yes">
  <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">

  <title>Spousebook Admin Login</title>
 
 <link rel="stylesheet" href="{{ url('admin_spo/css') }}/bootstrap.min.css"/>
  <link rel="stylesheet" href="{{ url('admin_spo/css') }}/dashboard.css"/>
</head>
<body>
<div class="login-bg"></div>

<div class="tab-content">
  <!-- //Login section -->
  <div role="tabpanel" class="tab-pane fade in active" id="loginSection">
    <div class="login-mainbox">
      <form class="loginbox" action="dashboard.html">
        <div class="login-logobox">
          <img src="images/logo.png" alt="Logo">
        </div>
        <div class="login-textbox">
          Signin to Admin
        </div>
        <div class="form-group form-group-lg mt15">
          <input type="text" class="form-control" placeholder="Username">
        </div>
        <div class="form-group form-group-lg">
          <input type="password" class="form-control" placeholder="Password">
        </div>
        <div class="form-group mb0">
          <button class="btn btn-lg btn-success btn-block">Sign In</button>
        </div>
        <hr>
        <a href="#forgotSection" role="tab" data-toggle="tab">Forgot Password?</a>
      </form>
    </div>
  </div>

  <!-- //forgot password section -->
  <div role="tabpanel" class="tab-pane fade" id="forgotSection">
    <div class="login-mainbox">
      <form class="loginbox">
        <div class="login-logobox">
          <img src="images/logo.png" alt="Logo">
        </div>
        <div class="login-textbox">
          Reset forgot password
        </div>
        <div class="form-group form-group-lg mt15">
          <input type="text" class="form-control" placeholder="Enter your email address">
        </div>
        <div class="form-group mb0">
          <button class="btn btn-lg btn-success btn-block">Send</button>
        </div>
      </form>
    </div>
  </div>

</div>

<script src="{{ url('admin_spo/jquery.1.12.4.min.js') }}/jquery.validate.js"></script>
<script src="{{ url('admin_spo/bootstrap.min.js') }}/common.js"></script>
</body>
</html>