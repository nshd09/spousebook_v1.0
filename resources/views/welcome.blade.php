@extends('layouts.app')
@section('content')


<script type="text/ng-template" id="form-messages">
	  <div ng-message="required">This field cannot be blank</div>
	  <div ng-message="minlength">The value for this field is too short</div>
	  <div ng-message="maxlength">The value for this field is too long</div>
	  <div ng-message="email">You have entered an incorrect email value</div>
	  <div ng-message="pattern">You did not enter the value in the correct format</div>
	  <div ng-message="password-characters">
	    Your password must consist of alphabetical or numeric characters.
	    It must also contain at least one special character, a number as well as an uppercase letter.
	  </div>
</script>
<div class="container">
    <div class="row">
        <div class="col-sm-5 mt40">
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane fade in active" id="home">
                    <h1 class="text-theme text-black-shadow">New to Spousebook? <br><small>Create an account</small></h1>
                    <form name="regisform" class="form-horizontal" ng-controller="FormCtrl" ng-submit="submit(regisform.$valid)" novalidate autocomplete="off" id="registration">
                  
                         <div class="form-group form-group-lg">
                           <div class="col-sm-6">
                               <input type="text" placeholder="First Name"  class="form-control" name="fname" value="{{ old('name') }}" ng-model="fname" ng-minlength="2" ng-maxlength="20" required>
                               <div ng-messages="regisform.fname.$error" ng-messages-include="form-messages" ng-if="interacted(regisform.fname)" class="error-messages">
                                    <div ng-message="required">This field is required</div>
                                    <div ng-message="minlength">Letter should be more than 1</div>
                                    <div ng-message="maxlength">Letter should be less than 20</div>
                                </div>
                           </div>
                           <div class="col-sm-6">
                              <input type="text" placeholder="Last Name" class="form-control" name="lname" value="{{ old('lname') }}" ng-minlength="2" ng-maxlength="20" ng-model="lname" required >
                              
                              <div ng-messages="regisform.lname.$error" ng-messages-include="form-messages" ng-if="interacted(regisform.lname)" class="error-messages">
                                <div ng-message="required">This field is required</div>
                                <div ng-message="minlength">Letter should be more than 1</div>
                                <div ng-message="maxlength">Letter should be less than 20</div>
                            </div>		
                           </div>
                         </div>
                         <div class="form-group form-group-lg">
                           <div class="col-sm-12">
                             <input type="email" name="email" class="form-control" placeholder="Email address" id="emailId" ng-model="email" required>
                                <div ng-messages="regisform.email.$error" ng-messages-include="form-messages" ng-if="interacted(regisform.email)" class="error-messages">
                                        <div ng-message="required">This field is required</div>
                                  <div ng-message="email">Please enter correct email id</div>
                                      </div>
                             <span id="useremail"></span>
                           </div>
                         </div>

                        <div class="form-group form-group-lg">
                            <div class="col-sm-12">
                              <select class="form-control" name="community_id" ng-model="community_id" required>
                                <option value='' selected>Choose a Community</option>
                                  @if(!empty($comunnity))
                                    @foreach($comunnity as $row)
                                       <option value="{{ $row->community_id }}">{{ $row->community_description }}</option>
                                    @endforeach 
                                @endif
                                 
                              </select>
                              <div ng-messages="regisform.community_id.$error" ng-messages-include="form-messages" ng-if="interacted(regisform.community_id)" class="error-messages">
                                      <div ng-message="required">This field is required</div>
                                    </div>
                            </div>
                        </div>
                        
                         <div class="form-group form-group-lg">
                           <div class="col-sm-12">
                             <input type="password" maxlength='12' name="password" ng-model="data.password" ng-minlength="6" ng-maxlength="12" password-characters-validator class="form-control" placeholder="Password" required>
                              <div ng-messages="regisform.password.$error" ng-if="interacted(regisform.password)" ng-messages-include="form-messages" class="error-messages">
                                <div ng-message="required">This field is required</div>
                                <div ng-message="minlength">Password length more than 5</div>
                                    <div ng-message="maxlength">Password length more than 12</div>
                                    <div ng-message="password-characters">
                                      Your password must consist of alphabetical or numeric characters.
                                       It must also contain at least one special character, a number as well as an uppercase letter.
                                    </div>
                              </div>
                           </div>
                             
                         </div>
                         <div class="form-group form-group-lg">
					    <div class="col-sm-12">
					      <input type="password"	
					      	name="passConfirm"
					      	ng-model="passConfirm"
					      	ng-minlength="6"
             			ng-maxlength="12"
             			password-characters-validator
             			match-validator="data.password"
					      	class="form-control"
					      	placeholder="Re enter Password" maxlength='12'>
					      	<div class="error-messages" ng-if="interacted(regisform.passConfirm)" ng-messages="regisform.passConfirm.$error" ng-messages-include="form-messages">
						        <div ng-message="match">This password does not match the password entered before</div>
						      </div>
					    </div>
					  </div>

                                            <div class="form-group form-group-lg">
					  
                                                <div class="col-sm-12">
                                                  <input type="text" name="dob" ng-model="dob" class="form-control" placeholder="MM / DD / YYYY" datepicker >
                                                  <div ng-messages="regisform.dob.$error" ng-if="interacted(regisform.dob)" ng-messages-include="form-messages" class="error-messages">
                                                    <div ng-message="required">This field is required</div>
                                                  </div>
                                                </div>
					  </div>
                         <div class="form-group">
                           <div class="col-sm-12">
                             <label class="radio-inline">
                               <input type="radio" name="gender" id="inlineRadio1" ng-model="gender" value="m" required> Male
                               </label>
                               <label class="radio-inline">
                                 <input type="radio" name="gender" id="inlineRadio2" ng-model="gender" value="f" required> Female
                               </label>
                                <div ng-messages="regisform.gender.$error" ng-if="interacted(regisform.gender)" ng-messages-include="form-messages" class="error-messages">
					      	<div ng-message="required">This field is required</div>
					      </div>
                           </div>
                         </div>
                        
                        <input type="hidden" name="remember_token" value="<?php echo csrf_token(); ?>">
                        
                    <input type="hidden" name="role_id" value="3">
                         <div class="form-group">
                           <div class="col-sm-12">
                             <button type="submit" class="btn btn-warning btn-lg">Create an account</button>
                           </div>
                         </div>
                 
                </form>
                    <div>
                        <hr>
                        <a href="#forgotpass" data-toggle="tab" class="text-blue">Forgot Password?</a>
                    </div>
                </div>
                <!-- //section of forgot password -->
                <div role="tabpanel" class="tab-pane fade" id="forgotpass">
                    <h1 class="text-theme text-black-shadow">Forgot Password</h1>
                    <form class="form-horizontal" role="form" method="POST" action="{{ url('password/email') }}">
                        <div class="form-group form-group-lg">
                            <div class="col-sm-12">
                                <input type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="Reset">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            </div>
                        </div>
                        <div class="form-group form-group-lg">
                          <div class="col-sm-12">
                            <button type="submit" class="btn btn-warning btn-lg">Submit</button>
                          </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

