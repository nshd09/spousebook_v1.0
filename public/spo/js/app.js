// app.js
// create angular app
angular.module('app', ['ngMessages'])
  .controller('FormCtrl', function($scope) {

    $scope.submitted = false;
    $scope.submit = function(isValid) {
    	$scope.submitted = true;
    	if (isValid) {
            submitform();
    	}
    };
    $scope.interacted = function(field) {
      return $scope.submitted || field.$dirty;
    };
 })
  .directive('matchValidator', function() {
    return {
      require: 'ngModel',
      link : function(scope, element, attrs, ngModel) {
        ngModel.$parsers.push(function(value) {
          ngModel.$setValidity('match', value == scope.$eval(attrs.matchValidator));
          return value;
        });
      }
    };
  })
  .directive('passwordCharactersValidator', function() {
    var PASSWORD_FORMATS = [
      /[^\w\s]+/, //special characters
      /[A-Z]+/, //uppercase letters
      /\w+/, //other letters
      /\d+/ //numbers
    ];

    return {
      require: 'ngModel',
      link : function(scope, element, attrs, ngModel) {
        ngModel.$parsers.push(function(value) {
          var status = true;
          angular.forEach(PASSWORD_FORMATS, function(regex) {
            status = status && regex.test(value);
          });
          ngModel.$setValidity('password-characters', status);
          return value;
        });
      }
    };
	})

	// ///now directive for datepicker
	.directive('datepicker', function () {
    return {
      restrict: 'A',
      require: 'ngModel',
       link: function (scope, element, attrs, ngModelCtrl) {
       	var dateToday = new Date();
				var year = dateToday.getFullYear() - 18;
				dateToday.setFullYear(year);

        element.datepicker({
          dateFormat: 'mm/dd/yy',
          changeMonth: true,
          changeYear: true,
          yearRange: '1920:' + year + '',
          defaultDate: dateToday,
          controlType: 'select',
          onSelect: function (date) {
            scope.date = date;
            scope.$apply();
          }
        });
      }
    };
});