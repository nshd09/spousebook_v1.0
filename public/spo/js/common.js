// Start--- Hide and show left navigation bar 
    
var submitform = function(){
    var fields = $("#registration").serialize();
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    console.log(fields);
    $.ajax({
        url: 'users/register',
        type: 'post',
        data: fields,
        headers: {'X-CSRF-TOKEN': CSRF_TOKEN},
        success: function (data) {
           window.location.href = window.location.pathname+"home";
        },
        error: function (jqXHR, status, err) {
        },
    });
}


// End--- hide and...

//********************* open page using ajax ******************** /////
 $(document).ready(function () {
$('.pageId').click(function () {
    var pagename = $(this).data('page');
    $('#'+pagename).load('user/pages/'+pagename);

    });
});
// **********************end page ******************************* ////

//Start--table scrolling for horizontal on desktop view and not to work on mobile device;



$("#emailId").on("blur", function () {
        var email = $(this).val();
        
        $.ajax({
            url: 'checkEmail/'+email,
           // headers: {'X-CSRF-TOKEN': CSRF_TOKEN},
            type: 'GET',
            success: function (data) {
               if (data == "exist") {
                    $("#EmailId").val('');
                    $("#useremail").html('<label class="error">Username/Email already exist. Please try with another !</label>');
                } else {
                    $("#useremail").html('');
                }
            },
            error: function (jqXHR, status, err) {
             //alert('failure');
            },
        });
    });
    

    
    
    
    
///// basic information update ////////////////////////////////////


    function FrmController($scope, $http) {
                    $scope.errors = [];
                    $scope.msgs = [];

                    $scope.SignUp = function() {

                        $scope.errors.splice(0, $scope.errors.length); // remove all error messages
                        $scope.msgs.splice(0, $scope.msgs.length);

                        $http.post('post_es.php', {'uname': $scope.username, 'pswd': $scope.userpassword, 'email': $scope.useremail}
                        ).success(function(data, status, headers, config) {
                            if (data.msg != '')
                            {
                                $scope.msgs.push(data.msg);
                            }
                            else
                            {
                                $scope.errors.push(data.error);
                            }
                        }).error(function(data, status) { // called asynchronously if an error occurs
    // or server returns response with an error status.
                            $scope.errors.push(status);
                        });
                    }
                }
    
