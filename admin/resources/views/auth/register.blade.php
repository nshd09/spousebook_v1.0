@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-sm-5 mt40">
            <div class="tab-content">
            <div role="tabpanel" class="tab-pane fade in active" id="home">
                    <h1 class="text-theme text-black-shadow">New to Spousebook? <br><small>Create an account</small></h1>
                    <form class="form-horizontal" role="form" method="POST" action="{{ url('/register') }}">
               {!! csrf_field() !!}
                      <div class="form-group form-group-lg">
                        <div class="col-sm-6">
                           <input type="text" placeholder="First Name"  class="form-control" name="name" value="{{ old('name') }}">
                        </div>
                        <div class="col-sm-6">
                           <input type="text" placeholder="Last Name" class="form-control" name="lname" value="{{ old('lname') }}">
                        </div>
                      </div>
                      <div class="form-group form-group-lg">
                        <div class="col-sm-12">
                          <input type="text" name="email" class="form-control" placeholder="Email address" >
                        </div>
                      </div>
                     <!--  <div class="form-group form-group-lg">
                        <div class="col-sm-12">
                          <select class="form-control">
                            <option selected>Choose a community</option>
                            <option>Dating</option>
                            <option>Married</option>
                            <option>Divorced</option>
                            <option>Separated</option>
                            <option>Single</option>
                            <option>Common Law</option>
                            <option>Widow</option>
                            <option>Widower</option>
                          </select>
                        </div>
                      </div> -->
                      <div class="form-group form-group-lg">
                        <div class="col-sm-12">
                          <input type="password" name="password" class="form-control" id="inputPassword3" placeholder="Password" >
                        </div>
                      </div>
                      <div class="form-group form-group-lg">
                        <div class="col-sm-12">
                          <input type="password" name="password_confirmation" class="form-control" id="inputPassword3" placeholder="Re enter password" >
                        </div>
                      </div>
                     <!--  <div class="form-group form-group-lg">
                        <div for="inputEmail3" class="col-sm-12">Birthday</div>
                        <div class="col-sm-4">
                          <select name=day" class="form-control">
                            <option>Day</option>
                            <option>1</option>
                            <option>2</option>
                            <option>3</option>
                            <option>4</option>
                            <option>5</option>
                            <option>6</option>
                            <option>7</option>
                            <option>8</option>
                            <option>9</option>
                            <option>10</option>
                            <option>11</option>
                            <option>12</option>
                            <option>13</option>
                            <option>14</option>
                            <option>15</option>
                            <option>16</option>
                            <option>17</option>
                            <option>19</option>
                            <option>20</option>
                            <option>21</option>
                            <option>22</option>
                            <option>23</option>
                            <option>24</option>
                            <option>25</option>
                            <option>26</option>
                            <option>27</option>
                            <option>28</option>
                            <option>29</option>
                            <option>30</option>
                            <option>31</option>
                          </select>
                        </div>
                        <div name="month" class="col-sm-4">
                          <select class="form-control" required>
                            <option>Month</option>
                            <option>Jan</option>
                            <option>Feb</option>
                            <option>Mar</option>
                            <option>Apr</option>
                            <option>May</option>
                            <option>Jun</option>
                            <option>Jul</option>
                            <option>Aug</option>
                            <option>Sep</option>
                            <option>Oct</option>
                            <option>Nov</option>
                            <option>Dec</option>
                          </select>
                        </div>
                        <div name="year" class="col-sm-4">
                          <select class="form-control" required>
                            <option>Year</option>
                            <option>1950</option>
                            <option>1955</option>
                            <option>1960</option>
                            <option>1965</option>
                            <option>1970</option>
                            <option>1975</option>
                            <option>1980</option>
                            <option>1985</option>
                            <option>1990</option>
                            <option>1995</option>
                            <option>2000</option>
                            <option>2005</option>
                            <option>2010</option>
                            <option>2015</option>
                            <option>2016</option>
                          </select>
                        </div>
                      </div> -->
                      <div class="form-group">
                        <div class="col-sm-12">
                          <label class="radio-inline">
                                  <input type="radio" name="gender" id="inlineRadio1" value="m" > Male
                                </label>
                                <label class="radio-inline">
                                  <input type="radio" name="gender" id="inlineRadio2" value="f"> Female
                                </label>
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="col-sm-12">
                          <button type="submit" class="btn btn-warning btn-lg">Create an account</button>
                        </div>
                      </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
