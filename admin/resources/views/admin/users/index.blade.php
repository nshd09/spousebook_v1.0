@extends('admin.layouts.master')

@section('content')
<div class="row">
  <div class="col-sm-12">
    
    @if($users->count() > 0)

        <div class="tab-content tab-border">
          <div class="mb15">
            {!! link_to_route('users.create', trans('quickadmin::admin.users-index-add_new'), [], ['class' => 'btn btn-success']) !!}
            <hr>
          </div>

          <div role="tabpanel" class="tab-pane fade in active" id="datagrid">
            <table id="datatable" class="table table-striped table-bordered table-action">
              <thead>
                <tr>
                  <th>{{ trans('quickadmin::admin.users-index-name') }}</th>
                  <th>Email</th>
                  <th>&nbsp;</th>
                </tr>
              </thead>

              <tbody>
              @foreach ($users as $user)
                <tr>
                  <td>{{ $user->name }}</td>
                  <td>{{ $user->email }}</td>
                  <td>
                      {!! link_to_route('users.edit', trans('quickadmin::admin.users-index-edit'), [$user->id], ['class' => 'btn btn-xs btn-info']) !!}
                      {!! Form::open(['style' => 'display: inline-block;', 'method' => 'DELETE', 'onsubmit' => 'return confirm(\'' . trans('quickadmin::admin.users-index-are_you_sure') . '\');',  'route' => array('users.destroy', $user->id)]) !!}
                      {!! Form::submit(trans('quickadmin::admin.users-index-delete'), array('class' => 'btn btn-xs btn-danger')) !!}
                      {!! Form::close() !!}
                  </td>
                </tr>
              @endforeach
              </tbody>
            </table>
          </div>
        </div>

    @else
        {{ trans('quickadmin::admin.users-index-no_entries_found') }}
    @endif

  </div>
</div>

@endsection