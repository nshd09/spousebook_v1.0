@include('admin.partials.header')  <!-- //for header -->

  @include('admin.partials.sidebar')


  @if (Session::has('message'))
      <p>{{ Session::get('message') }}</p>
  @endif
  <div class="right-box g">
    <div class="right-box-inner">

      <!-- //this for breadcrumbs -->
      <div class="row">
        <div class="col-sm-12">
          <ol class="breadcrumb">
            <li>
              <a href="#">Home</a>
            </li>
            <li class="active">Dashboard</li>
          </ol>
        </div>
      </div>
      <!-- //this section for middle content -->
      
        @yield('content')
         

    </div><!--//close right-box-inner-->
  </div><!--//close right-box-->



@include('admin.partials.javascripts')

@yield('javascript')
@include('admin.partials.footer')