<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0,width=device-width,user-scalable=no">
  <meta name="apple-mobile-web-app-capable" content="yes">
  <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">

  <title>Spousebook Dashboard</title>
  <link href="{{ url('quickadmin/css') }}/bootstrap.min.css" rel="stylesheet">
  <!-- Custom styles for this template -->
  <link href="{{ url('quickadmin/css') }}/datatable.bootstrap.min.css" rel="stylesheet">
  <link href="{{ url('quickadmin/css') }}/dashboard.css" rel="stylesheet">
  <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<body>
<header class="top-header">
  <div class="container-fluid">
    <div class="row">
      <div class="col-xs-8 col-sm-4">
        <i class="mobile-nav fa fa-bars"></i>
        <a href="#"><img class="logo-img" src="{{ url('quickadmin/images') }}/logo.png"></a>
      </div>
      <div class="col-xs-4 col-sm-8 text-right">
        <div class="btn-group username-btn">
          <span type="button" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <img src="{{ url('quickadmin/images') }}/user.jpg"> <span class="caret"></span>
          </span>
          <ul class="dropdown-menu dropdown-common dropdown-menu-right">
            <li><a href="#"><i class="fa fa-cog"></i>Setting</a></li>
            <li><a href="#"><i class="fa fa-pencil-square-o"></i>Edit Profile</a></li>
            <li><a href="#"><i class="fa fa-file-text-o"></i>View Profile</a></li>
            <li role="separator" class="divider"></li>
            <li><a href="{{ url('logout') }}"><i class="fa fa-sign-out"></i>Log Out</a></li>
          </ul>
        </div>
      </div>
    </div>
  </div>
</header>

<div class="wrapper-box">