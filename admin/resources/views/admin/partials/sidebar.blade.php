<!-- //Left Box Start-->
<div class="left-box">
  <div class="left-navigation">
    <ul id="collapsing" role="tablist" aria-multiselectable="true">
      @if(Auth::user()->role_id == config('quickadmin.defaultRole'))
          <li @if(Request::path() == config('quickadmin.route').'/menu') class="panel panel-menu" @endif>
              <a href="{{ url(config('quickadmin.route').'/menu') }}">
                  <i class="fa fa-list"></i>
                  <span class="title">{{ trans('quickadmin::admin.partials-sidebar-menu') }}</span>
              </a>
          </li>
          <li @if(Request::path() == 'users') class="panel panel-menu" @endif>
              <a href="{{ url('users') }}">
                  <i class="fa fa-users"></i>
                  <span class="title">{{ trans('quickadmin::admin.partials-sidebar-users') }}</span>
              </a>
          </li>
          <li @if(Request::path() == 'roles') class="panel panel-menu" @endif>
              <a href="{{ url('roles') }}">
                  <i class="fa fa-gavel"></i>
                  <span class="title">{{ trans('quickadmin::admin.partials-sidebar-roles') }}</span>
              </a>
          </li>
          <li @if(Request::path() == config('quickadmin.route').'/actions') class="panel panel-menu" @endif>
              <a href="{{ url(config('quickadmin.route').'/actions') }}">
                  <i class="fa fa-users"></i>
                  <span class="title">{{ trans('quickadmin::admin.partials-sidebar-user-actions') }}</span>
              </a>
          </li>
          <li class="panel panel-menu">
              <a href="{{ url('admin/cms_page') }}">
                  <i class="fa fa-users"></i>
                  <span class="title">CMS Pages</span>
              </a>
          </li>
          @endif
          @foreach($menus as $menu)
            @if($menu->menu_type != 2 && is_null($menu->parent_id))
                @if(Auth::user()->role->canAccessMenu($menu))
                    <li @if(isset(explode('/',Request::path())[1]) && explode('/',Request::path())[1] == strtolower($menu->name)) class="panel panel-menu" @endif>
                        <a href="{{ route(config('quickadmin.route').'.'.strtolower($menu->name).'.index') }}">
                            <i class="fa {{ $menu->icon }}"></i>
                            <span class="title">{{ $menu->title }}</span>
                        </a>
                    </li>
                @endif
              @else
                @if(Auth::user()->role->canAccessMenu($menu) && !is_null($menu->children()->first()) && is_null($menu->parent_id))
                    <li>
                        <a href="#">
                            <i class="fa {{ $menu->icon }}"></i>
                            <span class="title">{{ $menu->title }}</span>
                            <span class="fa arrow"></span>
                        </a>
                        <ul class="sub-menu">
                            @foreach($menu['children'] as $child)
                                @if(Auth::user()->role->canAccessMenu($child))
                                    <li
                                            @if(isset(explode('/',Request::path())[1]) && explode('/',Request::path())[1] == strtolower($child->name)) class="active active-sub" @endif>
                                        <a href="{{ route(config('quickadmin.route').'.'.strtolower($child->name).'.index') }}">
                                            <i class="fa {{ $child->icon }}"></i>
                                            <span class="title">
                                                {{ $child->title  }}
                                            </span>
                                        </a>
                                    </li>
                                @endif
                            @endforeach
                        </ul>
                    </li>
                  @endif
              @endif
          @endforeach
          <li>
              <a href="{{ url('logout') }}">
                  <i class="fa fa-sign-out fa-fw"></i>
                  <span class="title">{{ trans('quickadmin::admin.partials-sidebar-logout') }}</span>
              </a>
          </li>
      </ul>
  </div>
</div>
<!-- //Left Box Close -->
