<?php echo $__env->make('admin.partials.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>  <!-- //for header -->

  <?php echo $__env->make('admin.partials.sidebar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>


  <?php if(Session::has('message')): ?>
      <p><?php echo e(Session::get('message')); ?></p>
  <?php endif; ?>
  <div class="right-box g">
    <div class="right-box-inner">

      <!-- //this for breadcrumbs -->
      <div class="row">
        <div class="col-sm-12">
          <ol class="breadcrumb">
            <li>
              <a href="#">Home</a>
            </li>
            <li class="active">Dashboard</li>
          </ol>
        </div>
      </div>
      <!-- //this section for middle content -->
      
        <?php echo $__env->yieldContent('content'); ?>
         

    </div><!--//close right-box-inner-->
  </div><!--//close right-box-->



<?php echo $__env->make('admin.partials.javascripts', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

<?php echo $__env->yieldContent('javascript'); ?>
<?php echo $__env->make('admin.partials.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>