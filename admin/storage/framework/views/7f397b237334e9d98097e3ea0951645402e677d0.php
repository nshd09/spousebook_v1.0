<?php $__env->startSection('content'); ?>
<div class="container">
    <div class="row">
        <div class="col-sm-5 mt40">
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane fade in active" id="home">
                    <h1 class="text-theme text-black-shadow">New to Spousebook? <br><small>Create an account</small></h1>
                    <?php echo Form::open(array('url' => 'Users/register', 'method' => 'post','class'=>'form-horizontal','id'=>'signup-form')); ?>   
                    <?php echo csrf_field(); ?>

                         <div class="form-group form-group-lg">
                           <div class="col-sm-6">
                               <input type="text" placeholder="First Name"  class="form-control" name="fname" value="<?php echo e(old('name')); ?>" required="required">
                           </div>
                           <div class="col-sm-6">
                              <input type="text" placeholder="Last Name" class="form-control" name="lname" value="<?php echo e(old('lname')); ?>" >
                           </div>
                         </div>
                         <div class="form-group form-group-lg">
                           <div class="col-sm-12">
                             <input type="email" name="email" class="form-control" placeholder="Email address" id="emailId" required="required">
                             <span id="useremail"></span>
                           </div>
                         </div>
                         <div class="form-group form-group-lg">
                           <div class="col-sm-12">
                             <select name="community_id" class="form-control select2 required" >
                               <option selected value=''>Choose a community</option>
                                <?php if(!empty($comunnity)): ?>
                                    <?php foreach($comunnity as $row): ?>
                                       <option value="<?php echo e($row->community_id); ?>"><?php echo e($row->community_description); ?></option>
                                    <?php endforeach; ?> 
                                <?php endif; ?>

                             </select>
                           </div>
                         </div>
                         <div class="form-group form-group-lg">
                           <div class="col-sm-12">
                             <input type="password" name="password" class="form-control" id="password" placeholder="Password" required="required">
                           </div>
                         </div>
                         <div class="form-group form-group-lg">
                           <div class="col-sm-12">
                             <input type="password" name="confirm_password" class="form-control" id="inputPassword3" placeholder="Re enter password" required="required"  equalTo="#password">
                           </div>
                         </div>
                         <div class="form-group form-group-lg">
                            <div for="inputEmail3" class="col-sm-12">Birthday</div>
                               <div class="col-sm-4">
                                 <select name="day" class="form-control select2 required" required="required">
                                     <option selected value="">Day</option>
                                   <option value="1">1</option>
                                   <option value="2">2</option>
                                   <option value="3">3</option>
                                   <option value="4">4</option>
                                   <option value="5">5</option>
                                   <option value="6">6</option>
                                   <option value="7">7</option>
                                   <option value="8">8</option>
                                   <option value="9">9</option>
                                   <option value="10">10</option>
                                   <option value="11">11</option>
                                   <option value="12">12</option>
                                   <option value="13">13</option>
                                   <option value="14">14</option>
                                   <option value="15">15</option>
                                   <option value="16">16</option>
                                   <option value="17">17</option>
                                   <option value="18">18</option>
                                   <option value="19">19</option>
                                   <option value="1">20</option>
                                   <option value="20">21</option>
                                   <option value="22">22</option>
                                   <option value="23">23</option>
                                   <option value="24">24</option>
                                   <option value="25">25</option>
                                   <option value="26">26</option>
                                   <option value="27">27</option>
                                   <option value="28">28</option>
                                   <option value="29">29</option>
                                   <option value="30">30</option>
                                   <option value="31">31</option>
                                 </select>
                               </div>
                               <div class="col-sm-4">
                                 <select name="month"  class="form-control select2 required" required="required">
                                   <option selected value="">Month</option>
                                   <option value="o1">Jan</option>
                                   <option value="02">Feb</option>
                                   <option value="03">Mar</option>
                                   <option value="04">Apr</option>
                                   <option value="05">May</option>
                                   <option value="06">Jun</option>
                                   <option value="07">Jul</option>
                                   <option value="08">Aug</option>
                                   <option value="09">Sep</option>
                                   <option value="10">Oct</option>
                                   <option value="11">Nov</option>
                                   <option value="12">Dec</option>
                                 </select>
                               </div>
                               <div class="col-sm-4">
                                 <select name="year" class="form-control select2 required" required>
                                   <option selected value="">Year</option>
                                   <option value="1950">1950</option>
                                   <option value="1955">1955</option>
                                   <option value="1960">1960</option>
                                   <option value="1965">1965</option>
                                   <option value="1970">1970</option>
                                   <option value="1975">1975</option>
                                   <option value="1980">1980</option>
                                   <option value="1985">1985</option>
                                   <option value="1990">1990</option>
                                   <option value="1995">1995</option>
                                   <option value="1950">2000</option>
                                   <option value="1950">2005</option>
                                   <option value="1950">2010</option>
                                   <option value="1950">2015</option>
                                   <option value="1950">2016</option>
                                 </select>
                               </div>
                        </div>
                         <div class="form-group">
                           <div class="col-sm-12">
                             <label class="radio-inline">
                               <input type="radio" name="gender" id="inlineRadio1" value="m" > Male
                               </label>
                               <label class="radio-inline">
                                 <input type="radio" name="gender" id="inlineRadio2" value="f"> Female
                               </label>
                           </div>
                         </div>
                    <input type="hidden" name="role_id" value="3">
                         <div class="form-group">
                           <div class="col-sm-12">
                             <button type="submit" class="btn btn-warning btn-lg">Create an account</button>
                           </div>
                         </div>
                    <?php echo Form::close(); ?>

                    <div>
                        <hr>
                        <a href="#forgotpass" data-toggle="tab" class="text-blue">Forgot Password?</a>
                    </div>
                </div>
                <!-- //section of forgot password -->
                <div role="tabpanel" class="tab-pane fade" id="forgotpass">
                    <h1 class="text-theme text-black-shadow">Forgot Password</h1>
                    <form class="form-horizontal" role="form" method="POST" action="<?php echo e(url('password/email')); ?>">
                        <div class="form-group form-group-lg">
                            <div class="col-sm-12">
                                <input type="email" class="form-control" name="email" value="<?php echo e(old('email')); ?>" placeholder="Reset">
                                <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
                            </div>
                        </div>
                        <div class="form-group form-group-lg">
                          <div class="col-sm-12">
                            <button type="submit" class="btn btn-warning btn-lg">Submit</button>
                          </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<?php $__env->stopSection(); ?>


<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>