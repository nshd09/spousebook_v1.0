<!-- //Left Box Start-->
<div class="left-box">
  <div class="left-navigation">
    <ul id="collapsing" role="tablist" aria-multiselectable="true">
      <?php if(Auth::user()->role_id == config('quickadmin.defaultRole')): ?>
          <li <?php if(Request::path() == config('quickadmin.route').'/menu'): ?> class="panel panel-menu" <?php endif; ?>>
              <a href="<?php echo e(url(config('quickadmin.route').'/menu')); ?>">
                  <i class="fa fa-list"></i>
                  <span class="title"><?php echo e(trans('quickadmin::admin.partials-sidebar-menu')); ?></span>
              </a>
          </li>
          <li <?php if(Request::path() == 'users'): ?> class="panel panel-menu" <?php endif; ?>>
              <a href="<?php echo e(url('users')); ?>">
                  <i class="fa fa-users"></i>
                  <span class="title"><?php echo e(trans('quickadmin::admin.partials-sidebar-users')); ?></span>
              </a>
          </li>
          <li <?php if(Request::path() == 'roles'): ?> class="panel panel-menu" <?php endif; ?>>
              <a href="<?php echo e(url('roles')); ?>">
                  <i class="fa fa-gavel"></i>
                  <span class="title"><?php echo e(trans('quickadmin::admin.partials-sidebar-roles')); ?></span>
              </a>
          </li>
          <li <?php if(Request::path() == config('quickadmin.route').'/actions'): ?> class="panel panel-menu" <?php endif; ?>>
              <a href="<?php echo e(url(config('quickadmin.route').'/actions')); ?>">
                  <i class="fa fa-users"></i>
                  <span class="title"><?php echo e(trans('quickadmin::admin.partials-sidebar-user-actions')); ?></span>
              </a>
          </li>
          <li class="panel panel-menu">
              <a href="<?php echo e(url('admin/cms_page')); ?>">
                  <i class="fa fa-users"></i>
                  <span class="title">CMS Pages</span>
              </a>
          </li>
          <?php endif; ?>
          <?php foreach($menus as $menu): ?>
            <?php if($menu->menu_type != 2 && is_null($menu->parent_id)): ?>
                <?php if(Auth::user()->role->canAccessMenu($menu)): ?>
                    <li <?php if(isset(explode('/',Request::path())[1]) && explode('/',Request::path())[1] == strtolower($menu->name)): ?> class="panel panel-menu" <?php endif; ?>>
                        <a href="<?php echo e(route(config('quickadmin.route').'.'.strtolower($menu->name).'.index')); ?>">
                            <i class="fa <?php echo e($menu->icon); ?>"></i>
                            <span class="title"><?php echo e($menu->title); ?></span>
                        </a>
                    </li>
                <?php endif; ?>
              <?php else: ?>
                <?php if(Auth::user()->role->canAccessMenu($menu) && !is_null($menu->children()->first()) && is_null($menu->parent_id)): ?>
                    <li>
                        <a href="#">
                            <i class="fa <?php echo e($menu->icon); ?>"></i>
                            <span class="title"><?php echo e($menu->title); ?></span>
                            <span class="fa arrow"></span>
                        </a>
                        <ul class="sub-menu">
                            <?php foreach($menu['children'] as $child): ?>
                                <?php if(Auth::user()->role->canAccessMenu($child)): ?>
                                    <li
                                            <?php if(isset(explode('/',Request::path())[1]) && explode('/',Request::path())[1] == strtolower($child->name)): ?> class="active active-sub" <?php endif; ?>>
                                        <a href="<?php echo e(route(config('quickadmin.route').'.'.strtolower($child->name).'.index')); ?>">
                                            <i class="fa <?php echo e($child->icon); ?>"></i>
                                            <span class="title">
                                                <?php echo e($child->title); ?>

                                            </span>
                                        </a>
                                    </li>
                                <?php endif; ?>
                            <?php endforeach; ?>
                        </ul>
                    </li>
                  <?php endif; ?>
              <?php endif; ?>
          <?php endforeach; ?>
          <li>
              <a href="<?php echo e(url('logout')); ?>">
                  <i class="fa fa-sign-out fa-fw"></i>
                  <span class="title"><?php echo e(trans('quickadmin::admin.partials-sidebar-logout')); ?></span>
              </a>
          </li>
      </ul>
  </div>
</div>
<!-- //Left Box Close -->
