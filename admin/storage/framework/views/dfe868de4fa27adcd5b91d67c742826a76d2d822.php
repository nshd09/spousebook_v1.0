<script src="<?php echo e(url('quickadmin/js')); ?>/jquery.1.12.4.min.js"></script>
<script src="<?php echo e(url('quickadmin/js')); ?>/bootstrap.min.js"></script>
<!-- this is for datatable -->
<script src="<?php echo e(url('quickadmin/js')); ?>/datatable.min.js"></script>
<script src="<?php echo e(url('quickadmin/js')); ?>/datatable.bootstrap.min.js"></script>
<script type="text/javascript">
$(document).ready(function() {
  $('#datatable').DataTable();
});
</script>
<script src="<?php echo e(url('quickadmin/js')); ?>/common.js"></script>