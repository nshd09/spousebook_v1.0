<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0,width=device-width,user-scalable=no">
  <meta name="apple-mobile-web-app-capable" content="yes">
  <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">

  <title>Spousebook Dashboard</title>
  <link href="<?php echo e(url('quickadmin/css')); ?>/bootstrap.min.css" rel="stylesheet">
  <!-- Custom styles for this template -->
  <link href="<?php echo e(url('quickadmin/css')); ?>/dashboard.css" rel="stylesheet">
  <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<body>
<div class="login-bg"></div>
<div class="tab-content">
  <!-- //Login section -->
  <div role="tabpanel" class="tab-pane fade in active" id="loginSection">
    <div class="login-mainbox">
            <div class="panel panel-default">
                <div class="login-textbox"><?php echo e(trans('quickadmin::auth.login-login')); ?></div>
                <div class="panel-body">
                    <?php if(count($errors) > 0): ?>
                        <div class="alert alert-danger">
                            <strong><?php echo e(trans('quickadmin::auth.whoops')); ?></strong> <?php echo e(trans('quickadmin::auth.some_problems_with_input')); ?>

                            <br><br>
                            <ul>
                                <?php foreach($errors->all() as $error): ?>
                                    <li><?php echo e($error); ?></li>
                                <?php endforeach; ?>
                            </ul>
                        </div>
                    <?php endif; ?>

                    <form class="loginbox"
                          role="form"
                          method="POST"
                          action="<?php echo e(url('login')); ?>">
                    <div class="login-logobox">
                    <img src="<?php echo e(url('quickadmin/images')); ?>/logo.png" alt="Logo">
                    </div> 
                    <div class="login-textbox">Signin to Admin</div>     
                        <input type="hidden"
                               name="_token"
                               value="<?php echo e(csrf_token()); ?>">

                        <div class="form-group form-group-lg mt15">
                                <input type="email"
                                       class="form-control" placeholder="Username"
                                       name="email"
                                       value="<?php echo e(old('email')); ?>">
                            
                        </div>

                        <div class="form-group form-group-lg">
                                <input type="password"
                                       class="form-control" placeholder="Password"
                                       name="password">
                        </div>

                        <div class="form-group mb0">
                                <button type="submit"
                                        class="btn btn-lg btn-success btn-block"
                                        style="margin-right: 15px;">
                                    <?php echo e(trans('quickadmin::auth.login-btnlogin')); ?>

                                </button>
                        </div>
                        <hr>
                        <a href="#forgotSection" role="tab" data-toggle="tab">Forgot Password?</a>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<?php echo $__env->make('admin.partials.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
