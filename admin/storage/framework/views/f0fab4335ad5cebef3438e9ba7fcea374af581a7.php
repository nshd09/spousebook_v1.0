<!DOCTYPE html>
<html>
<head>
    
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0,width=device-width,user-scalable=no">
  <meta name="apple-mobile-web-app-capable" content="yes">
  <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
  
  <title>Spousebook</title>

  <link rel="stylesheet" href="<?php echo e(url('spo/css')); ?>/bootstrap.min.css"/>
  <link rel="stylesheet" href="<?php echo e(url('spo/css')); ?>/style.css"/>
</head>
<body>

 <div class="loader">
  <div class="spinner">
    <div class="rect1"></div>
    <div class="rect2"></div>
    <div class="rect3"></div>
    <div class="rect4"></div>
    <div class="rect5"></div>
  </div>
</div> 
<?php if(!Auth::user()): ?>
<nav class="navbar navbar-inverse navbar-fixed-top">
  <div class="container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="http://www.xipetech.com/work/Spousebook/" title="Spousebook">
        <img src="<?php echo e(url('spo/images')); ?>/logo.png">
      </a>
    </div>
    <div id="navbar" class="navbar-collapse collapse">
      <form class="navbar-form navbar-right" method="post" action="<?php echo e(url('/login')); ?>">
        <?php echo csrf_field(); ?>

        <div class="form-group">
          <input type="text" class="form-control" placeholder="Email or Username" name="email" value="<?php echo e(old('email')); ?>">

        </div>
        <div class="form-group">
          <input type="password" class="form-control" name="password" placeholder="Password">
        </div>
        <button type="submit" class="btn btn-warning">Sign in</button>
        &nbsp;|&nbsp;
        <div class="btn-group">
          <button type="button" class="btn btn-default active dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            English <span class="caret"></span>
          </button>
          <ul class="dropdown-menu">
            <li><a href="#">French</a></li>
            <li><a href="#">German</a></li>
            <li><a href="#">Spanishn</a></li>
          </ul>
        </div>
      </form>
    </div>
  </div>
</nav>
<?php else: ?>

<nav class="navbar navbar-inverse navbar-fixed-top">
  <div class="container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="http://www.xipetech.com/work/Spousebook/" title="Spousebook">
        <img src="<?php echo e(url('spo/images')); ?>/logo.png" alt="Spousebook logo">
      </a>
    </div>
    <div id="navbar" class="navbar-collapse collapse">
      <ul class="nav navbar-nav navbar-right">
        <li><a href="#"><span class="glyphicon glyphicon-exclamation-sign"></span> Notifications</a></li>
        <li><a href="#"><span class="glyphicon glyphicon-cog"></span> Settings</a></li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><span class="glyphicon glyphicon-user"></span> <?php echo e(ucfirst($userProfile->fname)); ?> <span class="caret"></span></a>
          <ul class="dropdown-menu dropdown-menu-left">
            <li><a href="<?php echo e(url('users/profile/',Auth::user()->id)); ?>">Profile</a></li>
            <li><a href="<?php echo e(url('users/setting/',Auth::user()->id)); ?>">Setting</a></li>
            <li><a href="<?php echo e(url('logout')); ?>">Logout</a></li>
          </ul>
        </li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">English <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="#">French</a></li>
            <li><a href="#">German</a></li>
            <li><a href="#">Spanishn</a></li>
          </ul>
        </li>
      </ul>
    </div>
  </div>
</nav>

<?php endif; ?>




<div class="image-set"></div>



<?php echo $__env->yieldContent('content'); ?>






<footer class="footer">
  <div class="container">
    <div class="row">
      <div class="col-md-12 text-center">
        <ul>
          <li><a href="#">About Us</a></li>
          <li><a href="#">Privacy</a></li>
          <li><a href="#">Terms</a></li>
          <li><a href="#">FAQ</a></li>
          <li><a href="#">Invite</a></li>
        </ul>
        <p class="copy-rights">&copy; 2016 Spousebook, All Rights Reserved.</p>
      </div>
    </div>
  </div>
</footer>


<script src="//code.jquery.com/jquery-1.11.3.min.js"></script>
<script src="<?php echo e(url('quickadmin/js')); ?>/bootstrap.min.js"></script>
<script src="<?php echo e(url('spo/js')); ?>/jquery.validate.js"></script>
<script src="<?php echo e(url('spo/js')); ?>/common.js"></script>
<script type="text/javascript">
  $('#signup-form').validate();
 
</script>




<script type="text/javascript">
  // page loader 
$(window).load(function(){
  $('.loader').fadeOut();
});


// on body scroll then fixed header on top
$(window).scroll(function(){
  var sticky = $('header'),
  scroll = $(window).scrollTop();
  if (scroll >= 100){
    sticky.addClass('fixed-header');
    $('.nav-left-part, .nav-right-part, .search-social').removeClass('common');
  }
  else{
    sticky.removeClass('fixed-header');
  }
});

// search box open and when click on body then search box hide
$(document).ready(function() {
  $(".open-serach-box").click(function(e) {
    $('.search-form').fadeToggle();
    e.stopPropagation();
  });
  $(document).click(function(e) {
    if (!$(e.target).is('.search-form, .search-form *')) {
      $(".search-form").fadeOut();
    }
  });
});

// this is mobile navigation;
$(document).ready(function() {
  $(".mobile-nav").click(function() {
    $('.nav-left-part, .nav-right-part, .search-social').toggleClass('common');
  });
});


//fixed left side social ans aponsured icons
$(window).on('scroll', function(){
  var getoff = $('#leftMove').offset().top;
  var findontop = $(this).scrollTop();
  var fixedboxheight = $('#rightFixed').innerHeight();
  var fixedleft = $('#rightSticky').offset().left;
  var rightWidth = $('#rightFixed').width();
  var footeroff = $('#scrollEndGoTop').offset().top;
  if (findontop>=getoff-35) {
    $('#rightFixed').css({
      'position':'fixed',
      'top':'35px',
      'left' : (fixedleft+15)+'px',
      'width' : rightWidth+'px',
    });
    if(findontop>=(footeroff-fixedboxheight-35)) {
      $('#rightFixed').css({
        'position':'fixed',
        'top':-findontop+footeroff-fixedboxheight+'px',
        'left' : (fixedleft+15)+'px',
        'width' : rightWidth+'px',
      });
    }
  }
  else{
    $('#rightFixed').css({
      'position':'relative',
      'left':'0px',
      'top':'0px',
    })
  }
});
</script>
</body>
</html>