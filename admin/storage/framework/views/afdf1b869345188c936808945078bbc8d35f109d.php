<?php $__env->startSection('content'); ?>
<div class="row">
  <div class="col-sm-12">
    
    <?php if($users->count() > 0): ?>

        <div class="tab-content tab-border">
          <div class="mb15">
            <?php echo link_to_route('users.create', trans('quickadmin::admin.users-index-add_new'), [], ['class' => 'btn btn-success']); ?>

            <hr>
          </div>

          <div role="tabpanel" class="tab-pane fade in active" id="datagrid">
            <table id="datatable" class="table table-striped table-bordered table-action">
              <thead>
                <tr>
                  <th><?php echo e(trans('quickadmin::admin.users-index-name')); ?></th>
                  <th>Email</th>
                  <th>&nbsp;</th>
                </tr>
              </thead>

              <tbody>
              <?php foreach($users as $user): ?>
                <tr>
                  <td><?php echo e($user->name); ?></td>
                  <td><?php echo e($user->email); ?></td>
                  <td>
                      <?php echo link_to_route('users.edit', trans('quickadmin::admin.users-index-edit'), [$user->id], ['class' => 'btn btn-xs btn-info']); ?>

                      <?php echo Form::open(['style' => 'display: inline-block;', 'method' => 'DELETE', 'onsubmit' => 'return confirm(\'' . trans('quickadmin::admin.users-index-are_you_sure') . '\');',  'route' => array('users.destroy', $user->id)]); ?>

                      <?php echo Form::submit(trans('quickadmin::admin.users-index-delete'), array('class' => 'btn btn-xs btn-danger')); ?>

                      <?php echo Form::close(); ?>

                  </td>
                </tr>
              <?php endforeach; ?>
              </tbody>
            </table>
          </div>
        </div>

    <?php else: ?>
        <?php echo e(trans('quickadmin::admin.users-index-no_entries_found')); ?>

    <?php endif; ?>

  </div>
</div>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>