<?php $__env->startSection('content'); ?>

    <div class="row">
        <div class="col-sm-10 col-sm-offset-2">
            <h1><?php echo e(trans('quickadmin::qa.menus-createParent-create_new_parent')); ?></h1>

            <?php if($errors->any()): ?>
                <div class="alert alert-danger">
                    <ul>
                        <?php echo implode('', $errors->all('
                        <li class="error">:message</li>
                        ')); ?>

                    </ul>
                </div>
            <?php endif; ?>
        </div>
    </div>

    <?php echo Form::open(['class' => 'form-horizontal']); ?>


    <div class="form-group">
        <?php echo Form::label('title', trans('quickadmin::qa.menus-createParent-parent_title'), ['class'=>'col-sm-2 control-label']); ?>

        <div class="col-sm-10">
            <?php echo Form::text('title', old('title'), ['class'=>'form-control', 'placeholder'=> trans('quickadmin::qa.menus-createParent-parent_title_placeholder')]); ?>

        </div>
    </div>

    <div class="form-group">
        <?php echo Form::label('roles', trans('quickadmin::qa.menus-createParent-roles') , ['class'=>'col-sm-2 control-label']); ?>

        <div class="col-sm-10">
            <?php foreach($roles as $role): ?>
                <div>
                    <label>
                        <?php echo Form::checkbox('roles['.$role->id.']',$role->id,old('roles.'.$role->id)); ?>

                        <?php echo $role->title; ?>

                    </label>
                </div>
            <?php endforeach; ?>
        </div>
    </div>

    <div class="form-group">
        <?php echo Form::label('icon', trans('quickadmin::qa.menus-createParent-icon') , ['class'=>'col-sm-2 control-label']); ?>

        <div class="col-sm-10">
            <?php echo Form::text('icon', old('icon','fa-database'), ['class'=>'form-control', 'placeholder'=> trans('quickadmin::qa.menus-createParent-icon_placeholder')]); ?>

        </div>
    </div>

    <div class="form-group">
        <div class="col-sm-10 col-sm-offset-2">
            <?php echo Form::submit( trans('quickadmin::qa.menus-createParent-create_parent') , ['class' => 'btn btn-primary']); ?>

        </div>
    </div>

    <?php echo Form::close(); ?>


<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>